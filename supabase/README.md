1. Install supabase CLI https://supabase.com/docs/guides/cli
2. Generate types https://supabase.com/docs/reference/javascript/typescript-support

```sh
supabase start
supabase gen types typescript --local > src/generated/supabase.types.ts
```
