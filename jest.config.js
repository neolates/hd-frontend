const path = require("path");

const paths = {
  APP_DIR: path.resolve(__dirname, "..", "src"),
};

exports.resolveRoot = [paths.APP_DIR, "node_modules"];

module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  roots: ["."],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  testPathIgnorePatterns: ["/node_modules/", "/.next/"],
  setupFilesAfterEnv: [],
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "ts-jest",
  },
  transformIgnorePatterns: ["/node_modules/", "^.+\\.module\\.(css|sass|scss)$"],
  moduleNameMapper: {
    "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy",
    "^@/(.*)$": "<rootDir>/src/$1",
  },
};
