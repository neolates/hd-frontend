# Useful commands

Read [Prisma migrate docs](https://www.prisma.io/docs/concepts/components/prisma-migrate/mental-model)

## Prototyping:

```sh
npx prisma db push
```

> May cause missing grants problem:
> https://supabase.com/docs/guides/integrations/prisma#missing-grants

So use instead

```sh
npx prisma migrate dev --create-only

npx prisma migrate reset
```

## Create migration:

```sh
npx prisma migrate dev
```

## Seeds:

```sh
npx prisma db seed
```

Read [prisma docs](https://www.prisma.io/docs/concepts/components/prisma-migrate/migrate-development-production) for details

# TODO: реализовать механизм раскатки dev баз из прода.

References:

- [https://supabase.com/docs/guides/integrations/snaplet]
