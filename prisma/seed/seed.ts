import { PrismaClient } from "@prisma/client";
import { randomUUID } from "crypto";
import getSlug from "speakingurl";

const prisma = new PrismaClient();
async function main() {
  // Generate example of discussion publication
  // const lang = "ru";
  // const theme = "Кто такие манифесторы?";
  // const createdAt = new Date();
  // const slug = `${getSlug(theme, {
  //   lang,
  // })}-${+createdAt}`;
  // const item = await prisma.publicationContent.upsert({
  //   where: { slug: "discussion-1" },
  //   update: {},
  //   create: {
  //     lang,
  //     articleType: "discussion",
  //     slug,
  //     theme,
  //     article: "Давайте обсудим кто такие манифесторы. Ваши мысли на этот счет?",
  //     authorId: randomUUID().toString(),
  //     readEstimateMinutes: 0,
  //     createdAt: createdAt,
  //   },
  // });
  // // Generate reading parts
  // const manager = new ReadingContentManager();
  // const data = manager.ProduceNewReadingContent(ReadingEditionType.Default, {
  //   langs: ["ru", "en", "id"],
  //   editionKey: "standard",
  // });
  // await prisma.readingPartContent.createMany({
  //   data,
  // });
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
