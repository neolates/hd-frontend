-- CreateTable
CREATE TABLE "chartContent" (
    "id" UUID NOT NULL,
    "lang" TEXT NOT NULL,
    "content_key" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "subtitle" TEXT NOT NULL DEFAULT '',
    "description" TEXT NOT NULL DEFAULT '',
    "next_ssg_enabled" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "chartContent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "publicationContent" (
    "id" UUID NOT NULL,
    "lang" TEXT NOT NULL,
    "article_type" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "theme" TEXT NOT NULL DEFAULT '',
    "article" TEXT NOT NULL DEFAULT '',
    "author_id" UUID NOT NULL,
    "read_estimate_minutes" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "next_ssg_enabled" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "publicationContent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "readingPartContent" (
    "id" UUID NOT NULL,
    "lang" TEXT NOT NULL,
    "edition_type" TEXT NOT NULL,
    "edition_key" TEXT NOT NULL,
    "content_key" TEXT NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "article" TEXT NOT NULL DEFAULT '',
    "read_estimate_minutes" INTEGER NOT NULL DEFAULT 0,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "readingPartContent_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "userProfile" (
    "user_id" UUID NOT NULL,
    "username" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "userProfile_pkey" PRIMARY KEY ("user_id")
);

-- CreateTable
CREATE TABLE "chart" (
    "id" UUID NOT NULL,
    "display_name" TEXT NOT NULL,
    "birth_timestamp" TIMESTAMP(3) NOT NULL,
    "birth_place_id" INTEGER NOT NULL,
    "timezone" TEXT NOT NULL,
    "owner_id" UUID NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "chart_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "chartReading" (
    "id" UUID NOT NULL,
    "chart_id" UUID NOT NULL,
    "edition_key" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "chartReading_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "discussion" (
    "id" UUID NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "created_by_id" UUID NOT NULL,

    CONSTRAINT "discussion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "discussionMember" (
    "id" UUID NOT NULL,
    "user_id" UUID NOT NULL,
    "join_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "discussionMember_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "discussionMessage" (
    "id" UUID NOT NULL,
    "content" TEXT NOT NULL,
    "send_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "sender_id" UUID NOT NULL,
    "discussion_id" UUID NOT NULL,

    CONSTRAINT "discussionMessage_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "chartContent_lang_content_key_key" ON "chartContent"("lang", "content_key");

-- CreateIndex
CREATE UNIQUE INDEX "publicationContent_slug_key" ON "publicationContent"("slug");

-- CreateIndex
CREATE UNIQUE INDEX "readingPartContent_lang_edition_key_content_key_key" ON "readingPartContent"("lang", "edition_key", "content_key");

-- CreateIndex
CREATE UNIQUE INDEX "chartReading_chart_id_edition_key_key" ON "chartReading"("chart_id", "edition_key");

-- AddForeignKey
ALTER TABLE "chart" ADD CONSTRAINT "chart_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "userProfile"("user_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chartReading" ADD CONSTRAINT "chartReading_chart_id_fkey" FOREIGN KEY ("chart_id") REFERENCES "chart"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "discussion" ADD CONSTRAINT "discussion_created_by_id_fkey" FOREIGN KEY ("created_by_id") REFERENCES "userProfile"("user_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "discussionMember" ADD CONSTRAINT "discussionMember_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "userProfile"("user_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "discussionMessage" ADD CONSTRAINT "discussionMessage_sender_id_fkey" FOREIGN KEY ("sender_id") REFERENCES "userProfile"("user_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "discussionMessage" ADD CONSTRAINT "discussionMessage_discussion_id_fkey" FOREIGN KEY ("discussion_id") REFERENCES "discussion"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
