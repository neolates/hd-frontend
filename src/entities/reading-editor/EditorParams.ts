import jwt from "jsonwebtoken";

export interface IEditorParams {
  lang: string;
  editionKey: string;
  contentKey: string;
}

const editorSecretKey = "<TODO REPLACE TO SECRET KEY>";

export function encodeEditorParams(params: IEditorParams): string {
  return jwt.sign(params, editorSecretKey);
}

export function decodeEditorParams(token: string): IEditorParams | undefined {
  try {
    return jwt.verify(token, editorSecretKey);
  } catch (err) {
    return;
  }
}
