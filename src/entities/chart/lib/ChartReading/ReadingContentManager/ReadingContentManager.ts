import { EditionKeysGenerator } from "../EditionKeysGenerator/EditionKeysGenerator";
import { ReadingEditionType, ReadingSections } from "../../../model/ChartReading";

interface NewReadingContentParams {
  langs: string[];
  editionKey: string;
}

interface NewReadingContent {
  editionType: string;
  editionKey: string;
  lang: string;
  contentKey: string;
}

export class ReadingContentManager {
  ProduceNewReadingContent(
    editionType: ReadingEditionType,
    params: NewReadingContentParams
  ): NewReadingContent[] {
    const generator = new EditionKeysGenerator(editionType);
    return ReadingSections.map((sectionKey) =>
      generator.generateContentKeys(sectionKey).map((contentKey) =>
        params.langs.map((lang) => ({
          editionType: editionType as string,
          editionKey: params.editionKey,
          lang,
          contentKey,
        }))
      )
    )
      .flat()
      .flat();
  }
}
