import { ReadingEditionType } from "@/entities";
import { ReadingContentManager } from "./ReadingContentManager";
import { equal } from "assert";

test("DefaultEditionReadingContentManagerProduceNewReadingContentTest", () => {
  const manager = new ReadingContentManager();
  let data;

  data = manager.ProduceNewReadingContent(ReadingEditionType.Default, {
    langs: ["en"],
    editionKey: "standard",
  });
  equal(data.length, 174);

  data = manager.ProduceNewReadingContent(ReadingEditionType.Default, {
    langs: ["ru"],
    editionKey: "standard",
  });
  equal(data.length, 174);

  data = manager.ProduceNewReadingContent(ReadingEditionType.Default, {
    langs: ["en", "ru"],
    editionKey: "standard",
  });
  equal(data.length, 348);
});
