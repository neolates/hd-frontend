import { ChartData, Interpretation, GateObject, GatesList, ActiveChannel } from "@/entities";

export function BuildChartData(): ChartData {
  return {
    active_channels: [],
    active_gates: [],
    gates_list: BuildChartDataGatesList(),
    defined_centers: [],
    dates: {
      personality_date: "",
      desing_date: "",
      p_julday: 0,
      d_julday: 0,
    },
    interpretation: BuildChartDataInerpretation(),
    variable: null,
  };
}

export function BuildChartDataInerpretation(): Interpretation {
  return {
    type: "",
    profile: 0,
    incarnation_cross: "",
    definition: "",
    inner_authority: "",
  };
}

export function BuildChartDataGateObject(): GateObject {
  return {
    type: "",
    planet_name: "",
    longitude: 0,
    format_lng: "",
    gate: 0,
    gate_percent: 0,
    line: 0,
    line_percent: 0,
    color: 0,
    color_percent: 0,
    tone: 0,
    tone_percent: 0,
    base: 0,
    base_percent: 0,
    fixing: "",
    fixing_planets: [],
  };
}

export function BuildChartDataGatesList(): GatesList {
  return {
    p_sun: BuildChartDataGateObject(),
    p_earth: BuildChartDataGateObject(),
    p_moon: BuildChartDataGateObject(),
    p_north_node: BuildChartDataGateObject(),
    p_south_node: BuildChartDataGateObject(),
    p_mercury: BuildChartDataGateObject(),
    p_venus: BuildChartDataGateObject(),
    p_mars: BuildChartDataGateObject(),
    p_jupiter: BuildChartDataGateObject(),
    p_saturn: BuildChartDataGateObject(),
    p_uranus: BuildChartDataGateObject(),
    p_neptune: BuildChartDataGateObject(),
    p_pluto: BuildChartDataGateObject(),
    d_sun: BuildChartDataGateObject(),
    d_earth: BuildChartDataGateObject(),
    d_moon: BuildChartDataGateObject(),
    d_north_node: BuildChartDataGateObject(),
    d_south_node: BuildChartDataGateObject(),
    d_mercury: BuildChartDataGateObject(),
    d_venus: BuildChartDataGateObject(),
    d_mars: BuildChartDataGateObject(),
    d_jupiter: BuildChartDataGateObject(),
    d_saturn: BuildChartDataGateObject(),
    d_uranus: BuildChartDataGateObject(),
    d_neptune: BuildChartDataGateObject(),
    d_pluto: BuildChartDataGateObject(),
  };
}

export function getGatesList(): number[] {
  return makeNumbersList(64);
}

export function getChannelsList(): ActiveChannel[] {
  return [
    [26, 44],
    [10, 20],
    [10, 34],
    [10, 57],
    [20, 34],
    [20, 57],
    [34, 57],
    [16, 48],
    [21, 45],
    [12, 22],
    [35, 36],
    [37, 40],
    [25, 51],
    [6, 59],
    [27, 50],
    [28, 38],
    [32, 54],
    [19, 49],
    [39, 55],
    [30, 41],
    [42, 53],
    [3, 60],
    [9, 52],
    [5, 15],
    [2, 14],
    [29, 46],
    [7, 31],
    [1, 8],
    [13, 33],
    [17, 62],
    [23, 43],
    [11, 56],
    [47, 64],
    [24, 61],
    [4, 63],
    [18, 58],
  ];
}

export function getCentersList(): string[] {
  return [
    "ROOT",
    "SACRAL",
    "SPLEEN",
    "SOLAR_PLEXUS",
    "HEART",
    "G_CENTER",
    "THROAT",
    "AJNA",
    "HEAD",
  ];
}

export function getTypesList(): string[] {
  return ["GENERATOR", "MANIFESTOR", "MANIFEST_GENERATOR", "PROJECTOR", "REFLECTOR"];
}

export function getAuthoritiesList(): string[] {
  return [
    "SOLAR_PLEXUS",
    "SACRAL",
    "SPLEEN",
    "EGO",
    "G_AUTHORITY",
    "EGO_PROJECTED",
    "NONE",
    "MOON",
  ];
}

export function getDefinitionsList(): string[] {
  return ["NONE", "SINGLE", "SPLIT", "TRIPLE_SPLIT", "QUADRUPLE_SPLIT"];
}

export function getProfilesList(): number[] {
  return [36, 14, 41, 63, 52, 13, 35, 25, 62, 46, 24, 51];
}

function makeNumbersList(n: number, first = 1) {
  return Array.from({ length: n }, (_, i) => i + 1);
}

export function getLinesList(): number[] {
  return makeNumbersList(6);
}

export function getColorsList(): number[] {
  return makeNumbersList(6);
}

export function getTonesList(): number[] {
  return makeNumbersList(6);
}
