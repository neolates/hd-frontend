import { BuildChartData, getCentersList } from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultCentersParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    const chartDefined = BuildChartData();
    chartDefined.defined_centers = [];
    const chartOpen = BuildChartData();
    chartOpen.defined_centers = getCentersList();
    return [chartDefined, chartOpen];
  }

  buildKeys() {
    return getCentersList();
  }
}
