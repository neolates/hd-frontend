import {
  getProfilesList,
  BuildChartData,
  BuildChartDataInerpretation,
} from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultProfileParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getProfilesList().map((profile) => ({
      ...BuildChartData(),
      interpretation: {
        ...BuildChartDataInerpretation(),
        profile,
      },
    }));
  }

  buildKeys() {
    return [];
  }
}
