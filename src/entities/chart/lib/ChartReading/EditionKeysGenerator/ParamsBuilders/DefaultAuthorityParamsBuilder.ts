import {
  getAuthoritiesList,
  BuildChartData,
  BuildChartDataInerpretation,
} from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultAuthorityParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getAuthoritiesList().map((inner_authority) => ({
      ...BuildChartData(),
      interpretation: {
        ...BuildChartDataInerpretation(),
        inner_authority,
      },
    }));
  }

  buildKeys() {
    return [];
  }
}
