import {
  getDefinitionsList,
  BuildChartData,
  BuildChartDataInerpretation,
} from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultDefinitionParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getDefinitionsList().map((definition) => ({
      ...BuildChartData(),
      interpretation: {
        ...BuildChartDataInerpretation(),
        definition,
      },
    }));
  }

  buildKeys() {
    return [];
  }
}
