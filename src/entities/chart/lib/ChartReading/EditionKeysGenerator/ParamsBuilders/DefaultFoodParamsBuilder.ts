import {
  getColorsList,
  getTonesList,
  BuildChartData,
  BuildChartDataGatesList,
  BuildChartDataGateObject,
} from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultFoodParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getColorsList()
      .map((color) => getTonesList().map((tone) => [color, tone]))
      .flat()
      .map(([color, tone]) => ({
        ...BuildChartData(),
        gates_list: {
          ...BuildChartDataGatesList(),
          d_sun: {
            ...BuildChartDataGateObject(),
            color,
            tone,
          },
        },
      }));
  }

  buildKeys() {
    return [];
  }
}
