import { BuildChartData, getChannelsList } from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultChannelsParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    const chart = BuildChartData();
    chart.active_channels = getChannelsList();
    return [chart];
  }

  buildKeys() {
    return getChannelsList().map((v) => v.join("-"));
  }
}
