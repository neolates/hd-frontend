import {
  getLinesList,
  BuildChartData,
  BuildChartDataGatesList,
  BuildChartDataGateObject,
} from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultGeneticTraumaParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getLinesList().map((line) => ({
      ...BuildChartData(),
      gates_list: {
        ...BuildChartDataGatesList(),
        d_mars: {
          ...BuildChartDataGateObject(),
          line,
        },
      },
    }));
  }

  buildKeys() {
    return [];
  }
}
