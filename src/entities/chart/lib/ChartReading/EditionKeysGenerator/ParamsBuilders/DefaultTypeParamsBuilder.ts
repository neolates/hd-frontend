import { getTypesList, BuildChartData, BuildChartDataInerpretation } from "../../ChartDataBuilder";
import { AbstractParamsBuilder } from "./AbstractParamsBuilder";

export class DefaultTypeParamsBuilder extends AbstractParamsBuilder {
  buildCharts() {
    return getTypesList().map((type) => ({
      ...BuildChartData(),
      interpretation: {
        ...BuildChartDataInerpretation(),
        type,
      },
    }));
  }

  buildKeys() {
    return [];
  }
}
