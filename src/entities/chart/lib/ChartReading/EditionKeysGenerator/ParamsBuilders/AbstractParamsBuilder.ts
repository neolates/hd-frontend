import { ChartData } from "../../../../model";
import { ParamsBuilder } from "../ParamsBuilderFactory";

export abstract class AbstractParamsBuilder implements ParamsBuilder {
  abstract buildCharts(): ChartData[];
  abstract buildKeys(): string[];

  build(sectionKey: string) {
    let charts = this.buildCharts();
    let keys = this.buildKeys();

    if (keys.length === 0) {
      return charts.map((chart) => ({
        chart,
        sectionKeys: [sectionKey],
      }));
    }

    return charts
      .map((chart) => keys.map((key) => ({ chart, key })))
      .flat()
      .map((v) => ({
        chart: v.chart,
        sectionKeys: [sectionKey, v.key],
      }));
  }
}
