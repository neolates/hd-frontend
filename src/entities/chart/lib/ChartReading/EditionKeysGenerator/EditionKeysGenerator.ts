import { ReadingEditionType } from "../../../model";
import { ContentKeyBuilder } from "../ContentKey/ContentKeyBuilder";
import { ParamsBuilder, ParamsBuilderFactory } from "./ParamsBuilderFactory";

export class EditionKeysGenerator {
  private paramsBuilders: Map<string, ParamsBuilder>;
  private contentKeyBuilder: ContentKeyBuilder;

  constructor(editionType: ReadingEditionType) {
    this.paramsBuilders = ParamsBuilderFactory.createBuilders(editionType);
    this.contentKeyBuilder = new ContentKeyBuilder(editionType);
  }

  generateContentKeys(sectionKey: string): string[] {
    const paramsBuilder = this.paramsBuilders.get(sectionKey);

    if (!paramsBuilder) {
      throw Error(`Could not find content resolver params resolver for sectionKey: ${sectionKey}`);
    }

    return paramsBuilder
      .build(sectionKey)
      .map((params) => this.contentKeyBuilder.Resolve(params))
      .filter((v) => v !== null)
      .map((v) => v as string)
      .filter((value, index, self) => self.indexOf(value) === index);
  }
}
