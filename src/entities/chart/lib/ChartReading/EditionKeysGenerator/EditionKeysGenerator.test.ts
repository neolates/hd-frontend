import { ReadingSection, ReadingEditionType, ReadingSections } from "@/entities";
import {
  getAuthoritiesList,
  getCentersList,
  getChannelsList,
  getColorsList,
  getDefinitionsList,
  getLinesList,
  getProfilesList,
  getTonesList,
  getTypesList,
} from "../ChartDataBuilder";
import { EditionKeysGenerator } from "./EditionKeysGenerator";
import { equal } from "assert";

const expectedKeysCount = {
  [ReadingSection.Type]: getTypesList().length,
  [ReadingSection.Strategy]: getTypesList().length,
  [ReadingSection.Profile]: getProfilesList().length,
  [ReadingSection.InWork]: getTypesList().length,
  [ReadingSection.InRelationship]: getTypesList().length,
  [ReadingSection.InChildhood]: getTypesList().length,
  [ReadingSection.HigherMeaning]: getTypesList().length,
  [ReadingSection.Habitat]: getColorsList().length * 2,
  [ReadingSection.GeneticTrauma]: getLinesList().length,
  [ReadingSection.Food]: getColorsList().length * getTonesList().length,
  [ReadingSection.FalseSelf]: getTypesList().length,
  [ReadingSection.Definition]: getDefinitionsList().length,
  [ReadingSection.Channels]: getChannelsList().length,
  [ReadingSection.Centers]: getCentersList().length * 2,
  [ReadingSection.Calling]: getLinesList().length,
  [ReadingSection.Authority]: getAuthoritiesList().length,
};

test("DefaultEditionContentKeysGeneratorTest", () => {
  const generator = new EditionKeysGenerator(ReadingEditionType.Default);

  ReadingSections.forEach((sectionKey) => {
    const keys = generator.generateContentKeys(sectionKey);
    equal(
      keys.length,
      expectedKeysCount[sectionKey],
      `Wrong keys count generated for section: ${sectionKey}`
    );
  });
});
