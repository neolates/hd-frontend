import { ChartData, ReadingEditionType, ReadingSection } from "../../../model";

import { DefaultAuthorityParamsBuilder } from "./ParamsBuilders/DefaultAuthorityParamsBuilder";
import { DefaultCallingParamsBuilder } from "./ParamsBuilders/DefaultCallingParamsBuilder";
import { DefaultCentersParamsBuilder } from "./ParamsBuilders/DefaultCentersParamsBuilder";
import { DefaultChannelsParamsBuilder } from "./ParamsBuilders/DefaultChannelsParamsBuilder";
import { DefaultDefinitionParamsBuilder } from "./ParamsBuilders/DefaultDefinitionParamsBuilder";
import { DefaultFoodParamsBuilder } from "./ParamsBuilders/DefaultFoodParamsBuilder";
import { DefaultGeneticTraumaParamsBuilder } from "./ParamsBuilders/DefaultGeneticTraumaParamsBuilder";
import { DefaultHabitatParamsBuilder } from "./ParamsBuilders/DefaultHabitatParamsBuilder";
import { DefaultProfileParamsBuilder } from "./ParamsBuilders/DefaultProfileParamsBuilder";
import { DefaultTypeParamsBuilder } from "./ParamsBuilders/DefaultTypeParamsBuilder";

import { ContentKeyBuilderParams } from "../ContentKey/ContentKeyBuilder";

export interface ParamsBuilder {
  buildCharts(): ChartData[];
  buildKeys(): string[];
  build(sectionKey: string): ContentKeyBuilderParams[];
}

export class ParamsBuilderFactory {
  static createBuilders(editionType: string): Map<string, ParamsBuilder> {
    const resolvers = new Map<string, ParamsBuilder>();

    // Добавляем новые типы прочтений
    if (editionType === ReadingEditionType.Default) {
      // Добавляем новые типы резолверов для разделов прочтений
      resolvers.set(ReadingSection.Type, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.Strategy, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.Profile, new DefaultProfileParamsBuilder());
      resolvers.set(ReadingSection.InWork, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.InRelationship, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.InChildhood, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.HigherMeaning, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.Habitat, new DefaultHabitatParamsBuilder());
      resolvers.set(ReadingSection.GeneticTrauma, new DefaultGeneticTraumaParamsBuilder());
      resolvers.set(ReadingSection.Food, new DefaultFoodParamsBuilder());
      resolvers.set(ReadingSection.FalseSelf, new DefaultTypeParamsBuilder());
      resolvers.set(ReadingSection.Definition, new DefaultDefinitionParamsBuilder());
      resolvers.set(ReadingSection.Channels, new DefaultChannelsParamsBuilder());
      resolvers.set(ReadingSection.Centers, new DefaultCentersParamsBuilder());
      resolvers.set(ReadingSection.Calling, new DefaultCallingParamsBuilder());
      resolvers.set(ReadingSection.Authority, new DefaultAuthorityParamsBuilder());
    } else {
      throw new Error(`Unsupported edition type: ${editionType}`);
    }

    return resolvers;
  }
}
