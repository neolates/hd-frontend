import { ChartData, ReadingSection } from "../../../model";
import {
  ContentKeyOrNull,
  ContentKeyResolver,
  ContentKeyResolverFactory,
} from "./ContentKeyResolverFactory";

export interface ContentKeyBuilderParams {
  chart: ChartData;
  sectionKeys: string[];
}

export class ContentKeyBuilder {
  private ContentKeyResolvers: Map<ReadingSection, ContentKeyResolver> | null = null;

  constructor(editionType: string) {
    this.ContentKeyResolvers = ContentKeyResolverFactory.createResolvers(editionType);
  }

  public Resolve({ chart, sectionKeys }: ContentKeyBuilderParams): ContentKeyOrNull {
    if (sectionKeys.length === 0) {
      throw new Error("Section keys must contain at least one item");
    }

    const section = sectionKeys[0] as ReadingSection;
    if (!Object.values(ReadingSection).includes(section)) {
      throw new Error(`Unknown reading section: ${sectionKeys[0]}`);
    }

    if (!this.ContentKeyResolvers) {
      throw new Error("Content resolvers are not initialized. Did you call WithEdition?");
    }

    const resolver = this.ContentKeyResolvers.get(section);

    if (!resolver) {
      throw new Error(`No resolver found for key: ${sectionKeys[0]}`);
    }

    return resolver.resolve(sectionKeys, chart);
  }
}
