import { ChartData, ReadingEditionType, ReadingSection } from "../../../model";
import {
  DefaultTypeContentKeyResolver,
  DefaultStrategyContentKeyResolver,
  DefaultProfileContentKeyResolver,
  DefaultInWorkContentKeyResolver,
  DefaultInRelationshipContentKeyResolver,
  DefaultInChildhoodContentKeyResolver,
  DefaultHigherMeaningContentKeyResolver,
  DefaultHabitatContentKeyResolver,
  DefaultGeneticTraumaContentKeyResolver,
  DefaultFoodContentKeyResolver,
  DefaultFalseSelfContentKeyResolver,
  DefaultDefinitionContentKeyResolver,
  DefaultChannelsContentKeyResolver,
  DefaultCentersContentKeyResolver,
  DefaultCallingContentKeyResolver,
  DefaultAuthorityContentKeyResolver,
} from "./ContentKeyResolvers/DefaultContentKeyResolvers";

export type ContentKeyOrNull = string | null;

export interface ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull;
}

export class ContentKeyResolverFactory {
  static createResolvers(editionType: string): Map<ReadingSection, ContentKeyResolver> {
    const resolvers = new Map<ReadingSection, ContentKeyResolver>();

    // Добавляем новые типы прочтений
    if (editionType === ReadingEditionType.Default) {
      // Добавляем новые типы резолверов для разделов прочтений
      resolvers.set(ReadingSection.Type, new DefaultTypeContentKeyResolver());
      resolvers.set(ReadingSection.Strategy, new DefaultStrategyContentKeyResolver());
      resolvers.set(ReadingSection.Profile, new DefaultProfileContentKeyResolver());
      resolvers.set(ReadingSection.InWork, new DefaultInWorkContentKeyResolver());
      resolvers.set(ReadingSection.InRelationship, new DefaultInRelationshipContentKeyResolver());
      resolvers.set(ReadingSection.InChildhood, new DefaultInChildhoodContentKeyResolver());
      resolvers.set(ReadingSection.HigherMeaning, new DefaultHigherMeaningContentKeyResolver());
      resolvers.set(ReadingSection.Habitat, new DefaultHabitatContentKeyResolver());
      resolvers.set(ReadingSection.GeneticTrauma, new DefaultGeneticTraumaContentKeyResolver());
      resolvers.set(ReadingSection.Food, new DefaultFoodContentKeyResolver());
      resolvers.set(ReadingSection.FalseSelf, new DefaultFalseSelfContentKeyResolver());
      resolvers.set(ReadingSection.Definition, new DefaultDefinitionContentKeyResolver());
      resolvers.set(ReadingSection.Channels, new DefaultChannelsContentKeyResolver());
      resolvers.set(ReadingSection.Centers, new DefaultCentersContentKeyResolver());
      resolvers.set(ReadingSection.Calling, new DefaultCallingContentKeyResolver());
      resolvers.set(ReadingSection.Authority, new DefaultAuthorityContentKeyResolver());
    } else {
      throw new Error(`Unsupported edition type: ${editionType}`);
    }

    return resolvers;
  }
}
