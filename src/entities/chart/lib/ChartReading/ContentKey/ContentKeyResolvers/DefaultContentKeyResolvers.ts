import { ChartData } from "../../../../model";
import { ContentKeyResolver, ContentKeyOrNull } from "../ContentKeyResolverFactory";

export class DefaultAuthorityContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    const authority = chart.interpretation?.inner_authority.replaceAll("_", "-").toLowerCase();
    return `authority-${authority}`;
  }
}

export class DefaultCallingContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    const line = chart.gates_list.d_mars.line;
    return `calling-design-mars-line-${line}`;
  }
}

export class DefaultCentersContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    if (sectionKeys.length === 1) {
      return `centers-intro`;
    }

    const centerKey = sectionKeys[1].replaceAll("_", "-").toLocaleLowerCase();
    const isDefined = chart.defined_centers?.some((v) => v === sectionKeys[1])
      ? "defined"
      : "not-defined";
    return `centers-${centerKey}-${isDefined}`;
  }
}

export class DefaultChannelsContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    if (sectionKeys.length === 1) {
      return `channels-intro`;
    }

    const activeChannels = chart.active_channels.map((c) => `${Math.min(...c)}-${Math.max(...c)}`);

    if (!activeChannels.some((v) => v === sectionKeys[1])) {
      return null;
    }
    return `channels-${sectionKeys[1]}`;
  }
}

export class DefaultDefinitionContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `definition-${chart.interpretation?.definition.replaceAll("_", "-").toLowerCase()}`;
  }
}

export class DefaultFalseSelfContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-false-self`;
  }
}

export class DefaultFoodContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    const color = chart.gates_list.d_sun.color;
    const tone = chart.gates_list.d_sun.tone;
    return `food-design-sun-color-${color}-tone-${tone}`;
  }
}

export class DefaultGeneticTraumaContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    const line = chart.gates_list.d_mars.line;
    return `genetic-trauma-design-mars-line-${line}`;
  }
}

export class DefaultHabitatContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    const color = chart.gates_list.d_north_node.color;
    const tone =
      Math.round(chart.gates_list.d_north_node.tone / 6 - 0.1) + 1 === 1 ? "left" : "right";
    return `habitat-design-north-node-color-${color}-${tone}`;
  }
}

export class DefaultHigherMeaningContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-higher-meaning`;
  }
}

export class DefaultInChildhoodContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-in-childhood`;
  }
}

export class DefaultInRelationshipContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-in-relationship`;
  }
}

export class DefaultInWorkContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-in-work`;
  }
}

export class DefaultProfileContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `profile-${chart.interpretation?.profile.toString().split("").join("-")}`;
  }
}

export class DefaultStrategyContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}-strategy`;
  }
}

export class DefaultTypeContentKeyResolver implements ContentKeyResolver {
  resolve(sectionKeys: string[], chart: ChartData): ContentKeyOrNull {
    return `type-${chart.interpretation?.type.replaceAll("_", "-").toLowerCase()}`;
  }
}
