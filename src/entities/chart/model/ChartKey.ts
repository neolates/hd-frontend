export interface NewChartQueryParams {
  year: string;
  month: string;
  day: string;
  hours: string;
  minutes: string;
  placeId: string;
  gender: string;
}

function stringify(v: NewChartQueryParams): string {
  // todo: add validation of query params
  return [
    v.year,
    (parseInt(v.month) + 1).toString().padStart(2, "0"),
    v.day.padStart(2, "0"),
    v.hours.padStart(2, "0"),
    v.minutes.padStart(2, "0"),
    v.gender ?? "0",
    v.placeId,
  ].join("");
}

function parse(v: string): NewChartQueryParams {
  const [, year, month, day, hours, minutes, gender, placeId] =
    v.match(/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})([m|f|0]{1})(\d+)/) ?? [];
  return {
    year,
    month,
    day,
    hours,
    minutes,
    gender,
    placeId,
  };
}

export const ChartKey = {
  stringify,
  parse,
};

export default ChartKey;
