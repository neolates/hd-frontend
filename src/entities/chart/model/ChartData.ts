/* eslint-disable */
import { definitions, paths } from "@/generated/bodygraph";
/* eslint-enable */

export type ChartData = paths["/bodygraph"]["get"]["responses"][200]["schema"];
export type ActiveChannel = definitions["ActiveChannel"];
export type Interpretation = definitions["Interpretation"];
export type GatesList = definitions["GatesList"];
export type GateObject = definitions["GateObject"];
