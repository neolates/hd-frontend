import { ReadingEditionType, ReadingSection } from "./ChartReading";

export type ReadingManagerListingData = Record<
  ReadingEditionType,
  Record<
    string,
    Record<
      ReadingSection,
      Record<
        string,
        {
          langs: string[];
          titles: Record<string, string>;
        }
      >
    >
  >
>;
