export enum ReadingEditionType {
  Default = "default",
}

export enum ReadingSection {
  Type = "type",
  Strategy = "strategy",
  Profile = "profile",
  InWork = "in-work",
  InRelationship = "in-relationship",
  InChildhood = "in-childhood",
  HigherMeaning = "higher-meaning",
  Habitat = "habitat",
  GeneticTrauma = "genetic-trauma",
  Food = "food",
  FalseSelf = "false-self",
  Definition = "definition",
  Channels = "channels",
  Centers = "centers",
  Calling = "calling",
  Authority = "authority",
}

export const ReadingSections = [
  ReadingSection.Type,
  ReadingSection.Strategy,
  ReadingSection.Profile,
  ReadingSection.InWork,
  ReadingSection.InRelationship,
  ReadingSection.InChildhood,
  ReadingSection.HigherMeaning,
  ReadingSection.Habitat,
  ReadingSection.GeneticTrauma,
  ReadingSection.Food,
  ReadingSection.FalseSelf,
  ReadingSection.Definition,
  ReadingSection.Channels,
  ReadingSection.Centers,
  ReadingSection.Calling,
  ReadingSection.Authority,
];
