"use client";
import Image from "next/image";

import { ReactElement } from "react";

/* eslint-disable */
import backgroundmd from "@assets/home/section/header/header-bg-md.svg";
import pattern from "@assets/home/section/header/header-bg-pattern.svg";
/* eslint-enable */

/* eslint-disable */
import SectionAmbassadors from "./widgets/SectionAmbassadors";
import SectionRelationship from "./widgets/SectionRelationship";
import SectionFooter from "./widgets/SectionFooter";
import SectionTypeCards from "./widgets/SectionTypeCards";
import SectionHeader from "./widgets/SectionHeader";
import { TopBar } from "@/widgets";
import { SectionCareer } from "./widgets/SectionCareer";
import { SectionLifeKeys } from "./widgets/SectionLifeKeys";
import { SectionForecast } from "./widgets/SectionForecast";
import { useRouter } from "next/navigation";

import "../../shared/styles/global.scss";
/* eslint-enable */

import "./page.scss";

function Page(): ReactElement {
  const { push } = useRouter();
  return (
    <>
      <div className="home-pic">
        <div className="home-pic-container">
          <Image src={backgroundmd} alt="" />
        </div>
      </div>
      <div className="page">
        <TopBar
          isSignedIn={false}
          onLogo={() => push("/")}
          onLogIn={() => push("/auth/signin")}
          onSignUp={() => push("/auth/signup")}
        />
        <SectionHeader />
        <SectionTypeCards />
        {/*
        <SectionDefault
          title="13 Spheres of Life Gene Keys"
          text="The Gene Keys are a living wisdom. These are teachings to imbibe, contemplate and apply in your daily life. As you allow them to percolate inside you, one by one your troubles will begin to fall away, and you will find yourself inhabiting a new and brighter life."
          imgUrlMd={spheresMdImg}
          imgUrlSm={spheresSmImg}
          btnText="Explore"
          btnDesc={`Calculate a personal\nchart for Free`}
          isReverse
        />
        */}
        <SectionLifeKeys
          title="13 Spheres of Life Gene Keys"
          text="The Gene Keys are a living wisdom. These are teachings to imbibe, contemplate and apply in your daily life. As you allow them to percolate inside you, one by one your troubles will begin to fall away, and you will find yourself inhabiting a new and brighter life."
          btnText="Explore"
          btnDesc={`Calculate a personal\nchart for Free`}
          imgHeightMd={745}
          isReverse
        />
        {/*
        <SectionDefault
          title="Career"
          text="This Report covers all 16 Success Codes, and provides you with the tools to understand your unique Career Design, and fully utilize the specific gifts, talents and attributes you are here to share with the world. Learn where and how you are best designed to be successful."
          imgUrlMd={careerMdImg}
          imgUrlSm={careerSmImg}
          imgHeightMd={800}
          btnText="Join Us"
          btnDesc={`Calculate a personal\nchart for Free`}
        />
        */}
        <SectionCareer
          title="Career"
          text="This Report covers all 16 Success Codes, and provides you with the tools to understand your unique Career Design, and fully utilize the specific gifts, talents and attributes you are here to share with the world. Learn where and how you are best designed to be successful."
          btnText="Join Us"
          imgHeightMd={877}
          btnDesc={`Calculate a personal\nchart for Free`}
        />
        <SectionRelationship />
        {/*
        <SectionDefault
          title="Forecast"
          text="The Gene Keys are a living wisdom. These are teachings to imbibe, contemplate and apply in your daily life. As you allow them to percolate inside you, one by one your troubles will begin to fall away, and you will find yourself inhabiting a new and brighter life."
          imgUrlMd={forecastMdImg}
          imgUrlSm={forecastSmImg}
          btnText="Explore"
          btnDesc={`Calculate a personal\nchart for Free`}
          isReverse
        />
        */}
        <SectionForecast
          title="Forecast"
          text="The Gene Keys are a living wisdom. These are teachings to imbibe, contemplate and apply in your daily life. As you allow them to percolate inside you, one by one your troubles will begin to fall away, and you will find yourself inhabiting a new and brighter life."
          btnText="Explore"
          imgHeightMd={799}
          btnDesc={`Calculate a personal\nchart for Free`}
          isReverse
        />
        <SectionAmbassadors />
        <SectionFooter />
      </div>
    </>
  );
}

export default Page;
