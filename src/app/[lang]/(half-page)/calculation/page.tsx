"use client";
import Link from "next/link";
import { AutocompleteField, Button, CheckboxField, TextField } from "@/shared/ui";

export default function SignUpPage() {
  return (
    <>
      <h1>Calculation</h1>
      <p className="text-gray-100 mt-[10px]">
        Enter your real information for the most accurate chart calculation. Enter your date of
        birth and place of birth
      </p>
      <div className="flex gap-5 flex-col mt-[40px] items-stretch ">
        <TextField name="day" variant="value-right" label="Day" />
        <AutocompleteField
          options={[{ text: "qwe", value: 123 }]}
          onSearch={async (v) => console.log(v)}
          variant="value-right"
          label="Month"
          aria-invalid={true}
        />
        <TextField name="day" variant="value-right" label="Day" />
        <TextField name="day" variant="value-right" label="Year" />
        <TextField name="day" variant="value-right" label="Time" />
        <AutocompleteField
          options={[{ text: "qwe", value: 123 }]}
          onSearch={async (v) => console.log(v)}
          variant="value-right"
          label="City"
        />
        <AutocompleteField
          options={[{ text: "qwe", value: 123 }]}
          onSearch={async (v) => console.log(v)}
          variant="value-right"
          label="Gender"
        />
        <CheckboxField label="I accept ">
          <Link href="/" className="underline">
            Terms of Use
          </Link>
        </CheckboxField>
      </div>
      <div className="flex gap-[30px] mt-[40px] flex-wrap items-center">
        <Button>Process</Button>
      </div>
    </>
  );
}
