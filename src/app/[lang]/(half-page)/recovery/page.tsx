"use client";
import Link from "next/link";
import { Button, TextField } from "@/shared/ui";

export default function SignUpPage() {
  return (
    <>
      <h1>Recover Pass</h1>
      <p className="text-gray-100 mt-[10px]">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
        Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes
      </p>
      <div className="flex gap-5 flex-col mt-[40px] items-stretch ">
        <TextField type="email" name="email" placeholder="Email" />
      </div>

      <div className="flex gap-[30px] mt-[40px] flex-wrap items-center">
        <Button>Send</Button>
        <Link href={"/"} className="underline">
          Resend instruction
        </Link>
      </div>
    </>
  );
}
