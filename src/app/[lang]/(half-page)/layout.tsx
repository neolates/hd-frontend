"use client";
import Image from "next/image";

import Link from "next/link";
import { PropsWithChildren } from "react";
import AuthBg from "@assets/auth-bg.svg";
import LogoIcon from "@assets/logo.svg";
import Rive from "@rive-app/react-canvas";

import "./layout.scss";

export default function AutLayout({ children }: PropsWithChildren) {
  return (
    <div className="flex bg-[#14101C] overflow-hidden min-h-screen">
      <Link
        href={"/"}
        className="absolute top-[44px] left-[20px] xl:top-[50px] xl:left-[50px] z-20">
        <Image src={LogoIcon} alt="logo" />
      </Link>
      <div className="xl:max-w-[720px] w-full  xl:p-[130px] p-5 pt-[134px] bg-transparent z-10">
        {children}
      </div>
      <div className="hidden relative xl:flex  w-full items-center justify-center ">
        <Rive
          src="/assets/animations/full.riv"
          className="absolute w-full h-full max-w-[900px] max-h-[900px] z-10 overflow-hidden"
        />
        <Image src={AuthBg} alt={"bg"} className="absolute h-full w-auto top-0 right-0 ml-auto" />
      </div>
    </div>
  );
}
