"use client";
import Link from "next/link";
import { usePinInput } from "react-pin-input-hook";
import { Button } from "@/shared/ui";

export default function VerificationPage() {
  const { fields } = usePinInput({
    onComplete: (value) => {
      console.log(value);
    },
    otp: true,
    type: "numeric",
    defaultValues: Array(6).fill(""),
    placeholder: "",
  });
  return (
    <>
      <h1>Verification</h1>
      <p className="text-gray-100 mt-[10px]">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
      </p>
      <div className="flex md:gap-[14px] gap-[5px] mt-[40px] items-center ">
        {fields.map((field, index) => (
          <input
            {...field}
            key={index}
            type="text"
            className="w-[50px] h-[60px] align-middle text-center text-[24px] font-[Poppins] bg-transparent border-2 rounded-[2px] outline-none border-[#332E3E] border-solid text-white"
          />
        ))}
      </div>
      <p className="mt-[40px]">
        Don’t receive the code?{" "}
        <Link href={"/"} className="text-goldenrod hover:text-white">
          Resend
        </Link>{" "}
        (56s)
      </p>
      <div className="flex gap-[30px] mt-[40px] flex-wrap items-center">
        <Button>Confirm</Button>
      </div>
    </>
  );
}
