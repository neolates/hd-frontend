export const dynamic = "force-dynamic";

import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";

import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

import { supabaseTypes } from "@/shared/lib";

export async function GET(request: NextRequest) {
  const requestUrl = new URL(request.url);
  const code = requestUrl.searchParams.get("code");

  if (code) {
    const supabase = createRouteHandlerClient<supabaseTypes.Database>({ cookies });
    const auth = await supabase.auth.exchangeCodeForSession(code);

    console.log("login: ", auth.data.user?.email);

    const { error } = await supabase
      .from("userProfile")
      .insert([
        {
          user_id: auth.data.user?.id,
          username: auth.data.user?.user_metadata?.signup?.username,
        },
      ])
      .select();
    console.log(error);
  }

  // URL to redirect to after sign in process completes
  return NextResponse.redirect(requestUrl.origin + "/auth/login");
}
