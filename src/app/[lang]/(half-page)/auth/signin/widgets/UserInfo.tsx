"use client";

import { createClientComponentClient, User } from "@supabase/auth-helpers-nextjs";

import { useEffect, useState } from "react";

import { supabaseTypes } from "@/shared/lib";

const UserInfo = () => {
  const supabase = createClientComponentClient<supabaseTypes.Database>();

  const [user, setUser] = useState<User | null>(null);
  const [isPending, setPending] = useState(true);

  useEffect(() => {
    supabase.auth.getUser().then((response) => {
      const {
        data: { user },
      } = response;
      setUser(user);
      setPending(false);
    });
  });

  if (isPending) {
    return <div>Loading...</div>;
  }

  if (!user) {
    return <div>Not authorized</div>;
  }

  return (
    <>
      <pre>{user?.id}</pre>
      <pre>{user?.email}</pre>
      <pre>{user.user_metadata["signup"].username}</pre>
    </>
  );
};

export default UserInfo;
