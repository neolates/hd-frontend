/* eslint-disable react/no-unescaped-entities */
/* eslint-disable import/no-internal-modules */
"use client";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useState } from "react";
import AppleWhite from "@assets/social/apple-white.svg";
import FacebookWhite from "@assets/social/facebook-white.svg";
import GoogleWhite from "@assets/social/google-white.svg";
import TwitterWhite from "@assets/social/twitter-white.svg";
import { supabaseTypes } from "@/shared/lib";
import { Button, TextField } from "@/shared/ui";
import { PhoneField } from "@/shared/ui/fields/PhoneField";

export default function Login() {
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const router = useRouter();
  const supabase = createClientComponentClient<supabaseTypes.Database>();

  const handleSignIn = async () => {
    // await supabase.auth.signInWithPassword({
    //   email,
    //   password,
    // });
    // router.refresh();
    console.log(phone, password);
  };

  // const handleSignOut = async () => {
  //   await supabase.auth.signOut();
  //   router.refresh();
  // };

  return (
    <>
      <h1>Sign in</h1>
      <p className="text-gray-100 mt-[10px]">
        Don't have an account yet?&nbsp;
        <Link href={"/auth/signup"} className="text-goldenrod hover:text-[#7F7F7F]">
          Sign Up
        </Link>
      </p>
      <div className="flex gap-5 flex-col mt-[40px]">
        <PhoneField label="Phone Number" value={phone} onChange={(e) => setPhone(e.target.value)} />
        <TextField
          type="password"
          name="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
          value={password}
        />
      </div>
      <div className="flex gap-[30px] mt-[40px] flex-wrap items-center">
        <Button onClick={handleSignIn}>Sign in</Button>
        <Link href={"/recovery"} className="underline">
          Forgot password?
        </Link>
      </div>
      <p className="mt-[20px] text-text-description">or login with</p>
      <div className="flex mt-[20px] gap-[4px]">
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={GoogleWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={AppleWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={FacebookWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={TwitterWhite} alt={"qwe"} />
        </div>
      </div>
    </>
  );
}
