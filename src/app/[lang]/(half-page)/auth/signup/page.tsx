/* eslint-disable import/no-internal-modules */
"use client";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import AppleWhite from "@assets/social/apple-white.svg";
import FacebookWhite from "@assets/social/facebook-white.svg";
import GoogleWhite from "@assets/social/google-white.svg";
import TwitterWhite from "@assets/social/twitter-white.svg";
import { supabaseTypes } from "@/shared/lib";
import { Button, CheckboxField, TextField } from "@/shared/ui";
import { PhoneField } from "@/shared/ui/fields/PhoneField";

export default function SignUpPage() {
  // const supabase = createClientComponentClient<supabaseTypes.Database>();
  const { push } = useRouter();
  const handleSignUp = async () => {
    // await supabase.auth.signInWithOtp({
    //   email,
    //   // password,
    //   options: {
    //     emailRedirectTo: `${location.origin}/auth/callback`,
    //     data: {
    //       signup: {
    //         username,
    //       },
    //     },
    //   },
    // });
    // router.refresh();
    push("/auth/verification");
  };
  return (
    <>
      <h1>Register</h1>
      <p className="text-gray-100 mt-[10px]">
        Already Have an Account?&nbsp;
        <Link href={"/auth/signin"} className="text-goldenrod hover:text-[#7F7F7F]">
          Sign In
        </Link>
      </p>
      <div className="flex gap-5 flex-col mt-[40px] items-stretch ">
        <PhoneField label="Phone Number" />
        <TextField type="text" name="name" placeholder="Your Name" />
        <TextField type="email" name="email" placeholder="Email" />
        <TextField type="password" name="password" placeholder="Password" />
        <CheckboxField label="By clicking continue you agree to ">
          <Link href={"/"} className="underline">
            Terms of Use
          </Link>
        </CheckboxField>
      </div>
      <div className="flex gap-[30px] mt-[40px] flex-wrap items-center">
        <Button onClick={handleSignUp}>Continue</Button>
      </div>
      <p className="mt-[20px] text-text-description">or login with</p>
      <div className="flex mt-[20px] gap-[4px]">
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={GoogleWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={AppleWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={FacebookWhite} alt={"qwe"} />
        </div>
        <div className="p-[13px] bg-[#1D1926] flex items-center w-[50px] h-[50px] justify-center">
          <Image src={TwitterWhite} alt={"qwe"} />
        </div>
      </div>
    </>
  );
}
