import Image from "next/image";

import React, { ReactElement } from "react";

import { Button } from "@/shared/ui";

/* eslint-disable */
import picture from "@assets/home/section/header/header-picture.svg";
/* eslint-enable */

import "./SectionHeader.scss";

function SectionHeader(): ReactElement {
  return (
    <div className="home-padding section-header">
      <div className="section-header-content text-white">
        <div className="section-header-headline">
          <p className="section-header-headline__subtitle">Discover Your Soul Map</p>
          <h1 className="section-header-headline__title">
            Human <span className="whitespace-nowrap">Design X</span>
          </h1>
        </div>

        <div className="section-header-text mt-3">
          <p className="text text-landing text-gray">
            Start with your energy type...<br></br>
            The first thing you look at in your chart is your Energy Type. There are five main
            types, and each has different way of bringing in more opportunities and flow into their
            life.
          </p>
          <div className="btncontainer">
            <Button className="btncontainer__btn" variant="default">
              Start
            </Button>
            <h4 className="btncontainer__desc text-description text-gray-dark">
              Calculate a personal
              <br />
              chart for Free
            </h4>
          </div>
        </div>
      </div>

      <div className="section-header-picture">
        <Image src={picture} alt="" fill></Image>
      </div>
    </div>
  );
}

export default SectionHeader;
