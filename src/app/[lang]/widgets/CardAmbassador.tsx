import clsx from "clsx";

import Image from "next/image";
import Link from "next/link";

import { Button } from "@/shared/ui";

import "./CardAmbassador.scss";

export interface AmbassadorCardProps {
  title: string;
  subtitle: string;
  description: string;
  imgUrl: string;
  followersCount: number;
  followersAvatars: string[];
  className?: string;
}

const CardAmbassador: React.FC<AmbassadorCardProps> = ({
  title,
  subtitle,
  description,
  imgUrl,
  followersCount,
  followersAvatars,
  className,
}) => (
  <div className={clsx(className, "ambassador-card__wrapper")}>
    <div className="ambassador-card__image-container">
      <Image className="ambassador-card__image" src={imgUrl} alt={title} fill />
    </div>

    <div className="ambassador-card__text">
      <div className="ambassador-card__text-headline">
        <h2 className="ambassador-card-title text-white">{title}</h2>
        <span className="ambassador-card-subtitle text-description text-white">{subtitle}</span>
      </div>

      <p className="text-landing text-white">{description}</p>

      <div className="ambassador-card-followers">
        <div className="ambassador-card-followers__col">
          <div className="ambassador-card-followers__count text-white text-description mb-1">
            Followers {followersCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
          </div>
          <div className="ambassador-card-followers__avatars">
            {followersAvatars.map((url, index) => (
              <Image
                key={index}
                className="ambassador-card__followers-avatars-item"
                src={url}
                alt=""
                width="40"
                height="40"
              />
            ))}
          </div>
        </div>
        <Link href="/" style={{ textDecoration: "none" }}>
          <Button variant="service" size="sm">
            Follow
          </Button>
        </Link>
      </div>
    </div>
  </div>
);

export default CardAmbassador;
