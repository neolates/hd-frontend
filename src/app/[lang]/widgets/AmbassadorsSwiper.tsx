"use client";

import React from "react";

import { EffectCards } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/effect-cards";

import CardAmbassador, { AmbassadorCardProps } from "./CardAmbassador";
import "./AmbassadorsSwiper.scss";

interface Props {
  items: AmbassadorCardProps[];
}

const AmbassadorsSwiper = ({ items }: Props) => {
  return (
    <>
      <Swiper
        effect={"cards"}
        grabCursor={true}
        modules={[EffectCards]}
        className="ambassador-swiper">
        {items.map((item, index) => (
          <SwiperSlide key={index}>
            <CardAmbassador
              title={item.title}
              subtitle={item.subtitle}
              description={item.description}
              imgUrl={item.imgUrl}
              followersCount={item.followersCount}
              followersAvatars={item.followersAvatars}
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default AmbassadorsSwiper;
