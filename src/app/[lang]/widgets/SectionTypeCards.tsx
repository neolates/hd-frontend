import { ReactElement } from "react";

/* eslint-disable */
import generator from "@assets/home/section/types/generator.svg";
import projectpr from "@assets/home/section/types/projector.svg";
import reflector from "@assets/home/section/types/reflector.svg";
import manifestor from "@assets/home/section/types/manifestor.svg";
import manifestingGenerator from "@assets/home/section/types/mainfesting-generator.svg";
/* eslint-enable */

import { SliderX, SliderXItem } from "@/shared/ui";

import TypeCard from "./TypeCard";

import "./SectionTypeCards.scss";

interface CardData {
  title: string;
  text: string;
  backgroundImg: string;
  href: string;
}

const cardData: CardData[] = [
  {
    title: "Generator",
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    backgroundImg: generator.src,
    href: '/typePage',
  },
  {
    title: "Projector",
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    backgroundImg: projectpr.src,
    href: '/typePage',
  },
  {
    title: "Reflector",
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    backgroundImg: reflector.src,
    href: '/typePage',
  },
  {
    title: "Manifestor",
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    backgroundImg: manifestor.src,
    href: '/typePage',
  },
  {
    title: "Manifesting Generator",
    text: "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    backgroundImg: manifestingGenerator.src,
    href: '/typePage',
  },
];

function SectionTypeCards(): ReactElement {
  return (
    <SliderX
      className="section-type-cards"
      firstGapClassName="section-type-cards-gap"
      lastGapClassName="section-type-cards-gap">
      {cardData.map((card, index) => (
        <SliderXItem key={index}>
          <TypeCard
            key={index}
            title={card.title}
            text={card.text}
            backgroundImg={card.backgroundImg}
            href={card.href}
          />
        </SliderXItem>
      ))}
    </SliderX>
  );
}

export default SectionTypeCards;
