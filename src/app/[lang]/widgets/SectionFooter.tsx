import Link from "next/link";

import { ReactElement } from "react";

import { Footer } from "@/widgets";

import { Button } from "@/shared/ui";

import "./SectionFooter.scss";

const links = [
  [
    { text: "Project", url: "/" },
    { text: "Generator", url: "/" },
    { text: "Reflector", url: "/" },
    { text: "Manifestor", url: "/" },
    { text: "Manifesting-Generator", url: "/" },
  ],
  [
    { text: "Pricing", url: "/" },
    { text: "Ambassador", url: "/" },
    { text: "Term of Use", url: "/" },
    { text: "Help Center", url: "/" },
  ],
  [
    { text: "Charts", url: "/" },
    { text: "Forecasts", url: "/" },
    { text: "Career", url: "/" },
    { text: "Profile", url: "/" },
  ],
];

function SectionFooter(): ReactElement {
  return (
    <div className="section section-footer px-2 mb-3">
      <div className="section-footer-links-container">
        <div className="section-footer-blocks-container">
          {links.map((items, index) => (
            <div key={index} className="section-footer-links-block">
              {items.map((item, index) => (
                <div key={index} className="section-footer-links-item">
                  <Link className="text-btn text-white" href={item.url}>
                    {item.text}
                  </Link>
                </div>
              ))}
            </div>
          ))}
        </div>

        <div className="section-footer-links-block section-footer-links-block--grow">
          <Button variant="outline">Have a question?</Button>
          <p className="text-description text-gray-dark mt-1 mb-3">Customer Service</p>
        </div>
      </div>

      <Footer />
    </div>
  );
}

export default SectionFooter;
