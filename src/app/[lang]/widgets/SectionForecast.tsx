/* eslint-disable boundaries/element-types */
/* eslint-disable import/no-internal-modules */
"use client";

import clsx from "clsx";
import Image from "next/image";
import smBg from "@assets/home/section/forecast-bd-sm.svg";
import bg from "@assets/home/section/forecast-bg-md.svg";
import body from "@assets/home/section/forecast-body-md.svg";
import useWindowDimensions from "@/app/hooks/useGetDimensions";
import { useRive } from "@rive-app/react-canvas";

import "./SectionForecast.scss";
import { Button } from "@/shared/ui";

type TSectionForecast = {
  title: string;
  text: string;
  btnText?: string;
  btnDesc?: string;
  isReverse?: boolean;
  imgHeightMd: number;
};

export const SectionForecast = ({
  imgHeightMd,
  isReverse,
  btnDesc,
  btnText,
  text,
  title,
}: TSectionForecast) => {
  const md = 1024;
  const { width } = useWindowDimensions();
  const { RiveComponent } = useRive({
    src: "/assets/animations/forecast.riv",
    autoplay: true,
  });
  return (
    <section className="section-default">
      <div className="section-default-img ">
        <Image
          className="rive-bg rive-bg--forecast"
          height={imgHeightMd}
          src={width && width >= md ? bg : smBg}
          alt=""
        />
        <div className="rive rive--forecast">
          <Image className="rive-forecast-body" src={body} alt="" />
          <RiveComponent />
        </div>
      </div>
      <div className="section-default-text-wrapper">
        <div
          className={clsx({
            "section-default-text-container": true,
            "section-default-text-container--reverse": isReverse,
          })}>
          <div
            className={clsx({
              "section-default-text-centrer section-default-text-center--forecast": true,
              "section-default-text-centrer--reverse": isReverse,
            })}>
            <div
              className={clsx({
                "section-default-text-inner": true,
                "section-default-text-inner--reverse": isReverse,
              })}>
              <h1 className="section-default-text-item text-white">
                <span className="section-default-text-item--title">{title}</span>
              </h1>
              <p className="section-default-text-item section-default-text-item--description text-landing text-gray">
                {text}
              </p>
              <div className="section-default-text-item btncontainer">
                {btnText && (
                  <Button className="btncontainer__btn" size="sm" variant="outline">
                    {btnText}
                  </Button>
                )}
                {btnDesc && (
                  <span className="btncontainer__desc text-gray-dark text-description">
                    {btnDesc}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
