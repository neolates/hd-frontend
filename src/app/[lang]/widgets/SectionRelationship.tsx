import Image from "next/image";

import { ReactElement } from "react";

/* eslint-disable */
import img1 from "@assets/home/section/relationship/relationship1.svg";
import img2 from "@assets/home/section/relationship/relationship2.svg";
import img3 from "@assets/home/section/relationship/relationship3.svg";
/* eslint-enable */

import { Button } from "@/shared/ui";

import "./SectionRelationship.scss";

const blocks = [
  {
    title: "Dating",
    description: "Receive more amazing and uplifting.",
    imgUrl: img1,
  },
  {
    title: "Family",
    description: "Receive more amazing and uplifting.",
    imgUrl: img2,
  },
  {
    title: "Business",
    description: "Receive more amazing and uplifting.",
    imgUrl: img3,
  },
];

function SectionRelationship(): ReactElement {
  return (
    <section className="section-relationship section">
      <div className="section-relationship-heading">
        <h1 className="text-white">Relationship</h1>
        <p className="text text-gray">
          Learn where and how you are best designed to be successful.
        </p>
      </div>

      <div className="relationship-list">
        {blocks.map((item, index) => (
          <div className="relationship-list-item" key={index}>
            <div className="relationship-list-item-img">
              <Image src={item.imgUrl} alt="" fill />
            </div>
            <div className="relationship-list-item-text">
              <h2 className="relationship-list-item-title text-white">{item.title}</h2>
              <p className="relationship-list-item-description text-description text-gray">
                {item.description}
              </p>
              <Button className="relationship-item-link" href="/" variant="outline" size="sm">
                {" "}
                Explore{" "}
              </Button>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
}

export default SectionRelationship;
