import clsx from "clsx";

import Image from "next/image";

import React from "react";

import { Button } from "@/shared/ui";

import "./SectionDefault.scss";

interface Props {
  title: string;
  text: string;
  imgUrlSm?: string;
  imgUrlMd?: string;
  imgHeightMd?: number;
  btnText?: string;
  btnDesc?: string;
  isReverse?: boolean;
}

const SectionDefault: React.FC<Props> = ({
  title,
  text,
  imgUrlSm,
  imgUrlMd,
  imgHeightMd,
  isReverse = false,
  btnText,
  btnDesc,
}) => {
  return (
    <section className="section-default">
      {imgUrlMd && (
        <Image className="section-default-img-md" height={imgHeightMd} src={imgUrlMd} alt="" />
      )}

      <div className="section-default-text-wrapper">
        <div
          className={clsx({
            "section-default-text-container": true,
            "section-default-text-container--reverse": isReverse,
          })}>
          <div
            className={clsx({
              "section-default-text-centrer": true,
              "section-default-text-centrer--reverse": isReverse,
            })}>
            <div
              className={clsx({
                "section-default-text-inner": true,
                "section-default-text-inner--reverse": isReverse,
              })}>
              <h1 className="section-default-text-item text-white">
                <span className="section-default-text-item--title">{title}</span>
              </h1>
              <p className="section-default-text-item section-default-text-item--description text-landing text-gray">
                {text}
              </p>
              <div className="section-default-text-item btncontainer">
                {btnText && (
                  <Button className="btncontainer__btn" size="sm" variant="outline">
                    {btnText}
                  </Button>
                )}
                {btnDesc && (
                  <span className="btncontainer__desc text-gray-dark text-description">
                    {btnDesc}
                  </span>
                )}
              </div>
            </div>
          </div>
          {imgUrlSm && (
            <div className="section-default-img-sm">
              <Image fill src={imgUrlSm} alt="section career" />
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default SectionDefault;
