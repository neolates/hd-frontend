/* eslint-disable boundaries/element-types */
/* eslint-disable import/no-internal-modules */
"use client";

import clsx from "clsx";
import Image from "next/image";
import bg from "@assets/home/section/career-bg-md.svg";
import smBg from "@assets/home/section/career-bg-sm.svg";
import useWindowDimensions from "@/app/hooks/useGetDimensions";
import { useRive } from "@rive-app/react-canvas";
import { Button } from "@/shared/ui";

import "./SectionDefault.scss";
import "./SectionCareer.scss";

export type TSectionCareer = {
  title: string;
  text: string;
  btnText?: string;
  btnDesc?: string;
  isReverse?: boolean;
  imgHeightMd: number;
};

export const SectionCareer = ({
  title,
  text,
  isReverse,
  btnDesc,
  btnText,
  imgHeightMd,
}: TSectionCareer) => {
  const md = 1024;
  const { width } = useWindowDimensions();
  const { RiveComponent } = useRive({
    src: "/assets/animations/career.riv",
    autoplay: true,
  });
  return (
    <section className="section-default">
      <div className="section-default-img ">
        <Image
          className="rive-bg rive-bg--career"
          height={imgHeightMd}
          src={width && width >= md ? bg : smBg}
          alt=""
        />
        <RiveComponent className="rive rive--career" />
      </div>
      <div className="section-default-text-wrapper">
        <div
          className={clsx({
            "section-default-text-container": true,
            "section-default-text-container--reverse": isReverse,
          })}>
          <div
            className={clsx({
              "section-default-text-centrer": true,
              "section-default-text-centrer--reverse": isReverse,
            })}>
            <div
              className={clsx({
                "section-default-text-inner": true,
                "section-default-text-inner--reverse": isReverse,
              })}>
              <h1 className="section-default-text-item text-white">
                <span className="section-default-text-item--title">{title}</span>
              </h1>
              <p className="section-default-text-item section-default-text-item--description text-landing text-gray">
                {text}
              </p>
              <div className="section-default-text-item btncontainer">
                {btnText && (
                  <Button className="btncontainer__btn" size="sm" variant="outline">
                    {btnText}
                  </Button>
                )}
                {btnDesc && (
                  <span className="btncontainer__desc text-gray-dark text-description">
                    {btnDesc}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
