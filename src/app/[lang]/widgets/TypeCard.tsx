import clsx from "clsx";

import Image from "next/image";

import "./TypeCard.scss";
import Link from "next/link";

interface CardProps {
  title: string;
  text: string;
  backgroundImg: string;
  className?: string;
  href: string
}

const TypeCard: React.FC<CardProps> = ({ title, backgroundImg, text, className, href }) => (
  <div className={clsx(className, "card-wrapper")}>
    <div className="card-wrapper__image-container">
      <Image
        className="card-wrapper__image"
        src={backgroundImg}
        alt={title}
        width="316"
        height="228"
      />
    </div>
    <div className="card-wrapper__text px-3 mb-3">
      <h3 className="card-wrapper__text-item text-white">{title}</h3>
      <p className="card-wrapper__text-item text-gray text-description">{text}</p>
      <Link href={href} className="card-wrapper__text-item text-btn text-white">
        Read More
      </Link>
    </div>
  </div>
);

export default TypeCard;
