/* eslint-disable import/no-internal-modules */
"use client";

import Image from "next/image";
import { ReactElement } from "react";

import { register as registerSwiper } from "swiper/element/bundle";

import bgMd from "@assets/home/section/ambassadors/md-bg.svg";
import { Button, SliderX, SliderXItem } from "@/shared/ui";

import AmbassadorsSwiper from "./AmbassadorsSwiper";

import CardAmbassador, { AmbassadorCardProps } from "./CardAmbassador";

import "./SectionDefault.scss";
import "./SectionAmbassadors.scss";

// register Swiper custom elements
registerSwiper();

const items: AmbassadorCardProps[] = [
  {
    title: "Ivan Polo",
    subtitle: "Profile 5/1 Heretic – Investigator",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    imgUrl: "/assets/ambassador-avatar-with-mask.svg",
    followersCount: 23988,
    followersAvatars: [
      "/assets/followers/follower-1.png",
      "/assets/followers/follower-2.png",
      "/assets/followers/follower-3.png",
      "/assets/followers/follower-4.png",
      "/assets/followers/follower-5.png",
    ],
  },
  {
    title: "Ivan Polo",
    subtitle: "Profile 5/1 Heretic – Investigator",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    imgUrl: "/assets/ambassador-avatar-with-mask.svg",
    followersCount: 23988,
    followersAvatars: [],
  },
  {
    title: "Ivan Polo",
    subtitle: "Profile 5/1 Heretic – Investigator",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing commodo ligula eget dolor. Aenean massa.",
    imgUrl: "/assets/ambassador-avatar-with-mask.svg",
    followersCount: 23988,
    followersAvatars: [],
  },
];

function SectionAmbassadors(): ReactElement {
  return (
    <div className="section-ambassadors section-default">
      <div className="section-ambassadors-img section-default-img-md">
        <Image src={bgMd} alt="" fill />
      </div>
      <div className="section-ambassadors section">
        <div className="section-default-text-inner">
          <h1 className="section-default-text-item text-white">
            <span className="section-default-text-item--title">Ambassador&#39;s</span>
          </h1>
          <p className="section-default-text-item section-default-text-item--description text-landing text-gray">
            This Report covers all 16 Success Codes, and provides you with the tools to understand
            your unique Career Design, and fully utilize the specific gifts, talents and attributes
            you are here to share with the world. Learn where and how you are best designed to be
            successful.
          </p>
          <div className="section-default-text-item btncontainer">
            <Button className="btncontainer__btn" size="sm" variant="outline">
              Start
            </Button>
            <span className="btncontainer__desc text-gray-dark text-description">
              Calculate a personal<br></br>chart for Free
            </span>
          </div>
        </div>

        <SliderX className="section-ambassadors-slider-mobile mb-5">
          {items.map((item, index) => (
            <SliderXItem key={index}>
              <CardAmbassador
                title={item.title}
                subtitle={item.subtitle}
                description={item.description}
                imgUrl={item.imgUrl}
                followersCount={item.followersCount}
                followersAvatars={item.followersAvatars}
              />
            </SliderXItem>
          ))}
        </SliderX>

        <AmbassadorsSwiper items={items}></AmbassadorsSwiper>
      </div>
    </div>
  );
}

export default SectionAmbassadors;
