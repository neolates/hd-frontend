import Link from "next/link";
import { notFound } from "next/navigation";

import { MDXRemote } from "next-mdx-remote/rsc";

import { Suspense } from "react";

import Chart from "@/widgets/bodygraph/Bodygraph";

import { ChartData, ContentKeyBuilder, ReadingEditionType, encodeEditorParams } from "@/entities";

import { http } from "@/shared/lib";

async function fetchChart(chartKey: string): Promise<http.ApiResponse<ChartData> | undefined> {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/bodygraph/${chartKey}`);
  if (!res.ok) return;
  return res.json();
}

async function fetchContent(lang: string, editionKey: string, contentKey: string) {
  const res = await fetch(
    `${http.getBaseApiUrl()}/api/v2/content/reading/${lang}/${editionKey}/${contentKey}`
  );
  if (!res.ok) return undefined;
  return res.json();
}

// export async function generateStaticParams() {
//   const records = await fetch(`${getBaseApiUrl()}/api/v2/content/chart/list`).then((res) =>
//     res.json()
//   );

//   return records.data.map(({ lang, contentKey }) => ({
//     lang,
//     contentKey,
//   }));
// }

export default async function Page({
  params,
}: {
  params: { lang: string; editionKey: string; chartKey: string; sectionKeys: string[] };
}) {
  const chartResponse = await fetchChart(params.chartKey);

  if (!chartResponse?.data) {
    notFound();
  }

  const contentKey = new ContentKeyBuilder(ReadingEditionType.Default).Resolve({
    chart: chartResponse.data,
    sectionKeys: params.sectionKeys,
  });

  if (contentKey === null) {
    notFound();
  }

  const content = await fetchContent(params.lang, params.editionKey, contentKey);

  if (!content) {
    notFound();
  }

  const editorPath = [
    "",
    params.lang,
    "chart/reading/editor",
    encodeEditorParams({
      lang: params.lang,
      editionKey: params.editionKey,
      contentKey,
    }),
  ].join("/");

  return (
    <div className="text-white">
      <p>{contentKey}</p>
      <p>{content.data.title}</p>
      <MDXRemote source={content.data.article.replaceAll("\n", "\\\n")}></MDXRemote>
      <Link href={editorPath}>Edit</Link>
      <Suspense fallback={<div>Loading...</div>}>
        <Chart chartKey={params.chartKey}></Chart>
      </Suspense>
    </div>
  );
}
