import { redirect } from "next/navigation";

export default async function Page({
  params,
}: {
  params: { lang: string; editionKey: string; chartKey: string };
}) {
  redirect(`/${params.lang}/chart/reading/${params.editionKey}/${params.chartKey}/type`);
}
