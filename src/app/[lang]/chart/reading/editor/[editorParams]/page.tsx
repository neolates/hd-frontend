import { notFound } from "next/navigation";

import { decodeEditorParams } from "@/entities";

import Editor from "./Editor";

export default async function Page({ params }: { params: { lang: string; editorParams: string } }) {
  const editorParams = decodeEditorParams(params.editorParams);
  if (!editorParams) {
    notFound();
  }

  return (
    <div className="text-white max-w-7xl mx-auto bg-gray-400 rounded-[24px] p-[30px]">
      <Editor {...editorParams}></Editor>
    </div>
  );
}
