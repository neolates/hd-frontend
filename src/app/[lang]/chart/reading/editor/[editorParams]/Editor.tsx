"use client";

import dynamic from "next/dynamic";

import React, { useEffect, useState, useTransition } from "react";

import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { PuffLoader, BarLoader } from "react-spinners";
import useSWR from "swr";

import { IEditorParams } from "@/entities";

import { http } from "@/shared/lib";

import { Button, MarkdownFieldProps, TextField } from "@/shared/ui";

export interface ReadingPartContentUpdateInput {
  title?: string;
  article?: string;
}

/* eslint-disable */
const MarkdownFieldComponent = dynamic(() => import("@/shared/ui/fields/MarkdownField"), {
  ssr: false,
  loading: () => <div>Loading...</div>,
});
/* eslint-enable */

const MarkdownField = React.forwardRef((props: MarkdownFieldProps, ref) => (
  <MarkdownFieldComponent {...props} forwardedRef={ref} />
));
MarkdownField.displayName = "MarkdownField";

export default function Editor(params: IEditorParams) {
  const contentPath = `/api/v2/content/reading/${params.lang}/${params.editionKey}/${params.contentKey}`;
  const { data, mutate, error } = useSWR(contentPath, http.fetcher<ReadingPartContentUpdateInput>);

  const { handleSubmit, control, watch, reset } = useForm<ReadingPartContentUpdateInput>();

  useEffect(() => {
    if (data) {
      reset(data);
      setDataChanged(false);
    }
  }, [data, reset]);

  const onSubmit: SubmitHandler<ReadingPartContentUpdateInput> = (data) => {
    // console.log(data);
    startTransition(() => saveChanges());
  };

  const [isPending, startTransition] = useTransition();

  const [isDataChanged, setDataChanged] = useState(false);

  const currentData = watch();

  useEffect(() => {
    // Note: You may want to do a deep equality check if the form data is complex
    setDataChanged(JSON.stringify(currentData) !== JSON.stringify(data));
  }, [currentData, data]);

  if (error) return <div>Failed to load</div>;
  if (!data)
    return (
      <div className="w-full">
        <BarLoader width={"100%"} height={1} color="#ffffff" speedMultiplier={0.5} />
      </div>
    );

  const saveChanges = async () => {
    try {
      await mutate(
        http.fetcher<ReadingPartContentUpdateInput>(contentPath, {
          method: "PUT",
          body: JSON.stringify(currentData),
        })
      );
    } catch (e) {
      alert(`Error saving update`);
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-full mx-auto flex flex-col items-center">
      <Controller
        name="title"
        control={control}
        rules={{ required: true }}
        render={({ field }) => <TextField {...field} className="my-1 w-full" />}
      />

      <Controller
        name="article"
        control={control}
        rules={{ required: true }}
        render={({ field }) => (
          <MarkdownField {...field} className="my-1 w-full"></MarkdownField>
        )}></Controller>

      <Button type="submit" disabled={!isDataChanged} className="w-[170px]">
        {isPending ? <PuffLoader color="#000000" size={30} /> : "Save Changes"}
      </Button>
    </form>
  );
}
