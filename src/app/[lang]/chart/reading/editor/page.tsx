import Link from "next/link";
import { notFound } from "next/navigation";

import {
  IEditorParams,
  ReadingManagerListingData,
  ReadingSections,
  encodeEditorParams,
} from "@/entities";

import { http } from "@/shared/lib";

async function fetchListing(): Promise<http.ApiResponse<ReadingManagerListingData> | undefined> {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/content/reading/manager/listing`, {
    next: { tags: ["reading-manager-listing"] },
  });
  if (!res.ok) return;
  return res.json();
}

export default async function Page({ params }: { params: { lang: string; editorParams: string } }) {
  const response = await fetchListing();
  // console.log(response);
  if (!response?.data) {
    return notFound();
  }

  function editorLink(params: IEditorParams): string {
    return `/${params.lang}/chart/reading/editor/${encodeEditorParams(params)}`;
  }
  return (
    <div className="text-white max-w-7xl mx-auto bg-gray-400 rounded-[24px] p-[30px]">
      {Object.entries(response.data).map(([editionType, editions]) => (
        <details key={editionType} className="select-none mb-1">
          <summary className="cursor-pointer marker:text-lg">
            <h1>{editionType}</h1>
          </summary>
          <div className="pl-2">
            {Object.entries(editions).map(([editionKey, editions]) => (
              <details key={editionKey} className="select-none mb-1">
                <summary className="cursor-pointer marker:text-lg">
                  <h2>{editionKey}</h2>
                </summary>
                <div className="pl-2">
                  {ReadingSections.map((sectionKey) => ({
                    sectionKey,
                    section: editions[sectionKey],
                  })).map(({ sectionKey, section }) => (
                    <details key={sectionKey} className="select-none mb-1">
                      <summary className="cursor-pointer marker:text-lg">
                        <h3>{sectionKey}</h3>
                      </summary>
                      <ul className="pl-2">
                        {Object.entries(section).map(([contentKey, content]) => (
                          <>
                            <li key={contentKey} className="flex justify-between">
                              <Link
                                href={editorLink({
                                  lang: params.lang,
                                  editionKey,
                                  contentKey,
                                })}>
                                {content.titles[params.lang] ?? contentKey}
                              </Link>
                              <div>
                                {content.langs.map((lang) => (
                                  <Link
                                    className="text-black bg-goldenrod hover:opacity-60 px-1 mx-1 inline-block rounded-full"
                                    key={lang}
                                    href={editorLink({ lang, editionKey, contentKey })}>
                                    {lang}
                                  </Link>
                                ))}
                              </div>
                            </li>
                            <hr className="w-full pb-[1px] my-1 bg-slate-500" />
                          </>
                        ))}
                      </ul>
                    </details>
                  ))}
                </div>
              </details>
            ))}
          </div>
        </details>
      ))}
    </div>
  );
}
