import { notFound, redirect } from "next/navigation";

import { ChartKey, NewChartQueryParams } from "@/entities";

import { http, base64Url } from "@/shared/lib";

async function fetchChart(chartKey: string) {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/bodygraph/${chartKey}`);
  if (!res.ok) return undefined;
  return res.json();
}

export default async function Page({
  params,
  searchParams,
}: {
  params: { lang: string };
  searchParams: NewChartQueryParams;
}) {
  const chartKey = base64Url.encode(ChartKey.stringify(searchParams));
  const { data: bodygraph } = await fetchChart(chartKey);
  if (!bodygraph) {
    notFound();
  }

  const chartType = bodygraph.interpretation.type.replace("_", "-").toLowerCase();
  const chartProfile = bodygraph.interpretation.profile.toString().split("").join("-");
  const contentKey = [chartType, chartProfile].join("-");

  redirect(`/${params.lang}/chart/${contentKey}/${chartKey}`);
}
