import { notFound } from "next/navigation";

import { Suspense } from "react";

import Chart from "@/widgets/bodygraph/Bodygraph";
import { fetchContent } from "./api";

// export async function generateStaticParams() {
//   const records = await fetch(`${getBaseApiUrl()}/api/v2/content/chart/list`).then((res) =>
//     res.json()
//   );

//   return records.data.map(({ lang, contentKey }) => ({
//     lang,
//     contentKey,
//   }));
// }

export default async function Page({
  params,
}: {
  params: { lang: string; contentKey: string; chartKey: string };
}) {
  const content = await fetchContent(params.lang, params.contentKey);
  if (!content) {
    notFound();
  }

  return (
    <div className="text-white">
      <h1>{content.data.title}</h1>
      <h2>{content.data.subtitle}</h2>
      <p>{content.data.description}</p>
      <Suspense fallback={<div>Loading...</div>}>
        <Chart chartKey={params.chartKey}></Chart>
      </Suspense>
    </div>
  );
}
