import { http } from "@/shared/lib";

export async function fetchChart(chartKey: string) {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/bodygraph/${chartKey}`);
  if (!res.ok) return undefined;
  return res.json();
}

export async function fetchContent(lang: string, contentKey: string) {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/content/chart/${lang}/${contentKey}`);
  if (!res.ok) return undefined;
  return res.json();
}
