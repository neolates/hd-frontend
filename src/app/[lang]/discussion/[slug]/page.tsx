// export const dynamic = "error";
// export const dynamicParams = true;

import { notFound } from "next/navigation";
import { fetchContent } from "./api";

// export async function generateStaticParams() {
//   const records = await fetch(`${getBaseApiUrl()}/api/v2/content/chart/list`).then((res) =>
//     res.json()
//   );

//   return records.data.map(({ lang, contentKey }) => ({
//     lang,
//     contentKey,
//   }));
// }

export default async function Page({ params }: { params: { lang: string; slug: string } }) {
  const content = await fetchContent(params.slug.slice(-36));
  if (!content) {
    notFound();
  }

  return (
    <div className="text-white">
      <h1>{content.theme}</h1>
      <div>{content.article}</div>
    </div>
  );
}
