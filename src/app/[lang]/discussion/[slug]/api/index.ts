import { publicationContent } from "@prisma/client";

import { http } from "@/shared/lib";

export async function fetchContent(id: string): Promise<publicationContent | undefined> {
  const res = await fetch(`${http.getBaseApiUrl()}/api/v2/content/publication/${id}`);
  if (!res.ok) return undefined;
  return res.json().then((v) => v.data);
}
