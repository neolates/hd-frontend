/* eslint-disable import/no-internal-modules */
"use client";
import Image from "next/image";

import { useRouter } from "next/navigation";
import manifastingBg from "@assets/types/section/header/manifesting-bg.svg";
import manifasting from "@assets/types/section/header/manifesting.svg";
import { SectionProfile, TopBar } from "@/widgets";

import SectionFooter from "../widgets/SectionFooter";
import SectionDiscussion from "./widgets/section-discussion";
import SectionFamousPeople from "./widgets/section-famous-people";
import SectionHeader from "./widgets/section-header";
import SectionTypeInfo from "./widgets/section-type-info";

import "./page.scss";

const Page = () => {
  const { push } = useRouter();
  return (
    <>
      <div className="type-pic">
        <div className="type-pic-container">
          <Image src={manifastingBg} alt="" />
        </div>
      </div>
      <div className="page">
        <TopBar isSignedIn={false} onLogo={() => push("/")} />
        <SectionHeader
          img={manifasting}
          title="Manifesting Generator"
          text="
            You are the most energetic personality type there is; you act decisively and without a second thought. The biggest challenge for you is to have patience and apprehension. The Manifesting Generator has the qualities of both the Manifestor and the Generator, and you will constantly be forced to struggle internally with what both types easily overcome: anger and frustration. Accepting your limitations and learning to wait… and slowly contemplate upon and weigh your options; this is simply not in your nature. You can easily initiate anything you like and immediately act upon your plans as you strive towards your goals. It is very common for the Manifesting Generator to work at several companies at once or have more than one source of income.
          "
          subtitle="Hyperfast Energy Provider"
        />
        <SectionFamousPeople peopleList={[]} />
        <SectionTypeInfo infoList={[]} />
        <SectionDiscussion
          discussionList={[
            {
              title: "asdf",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
            {
              title: "sdfasd",
            },
          ]}
        />
        <SectionProfile
          hrefLists={[
            [
              {
                lable: "1/3 Investigator – Martyr",
              },
              {
                lable: "1/3 Investigator – Martyr",
              },
              {
                lable: "1/3 Investigator – Martyr",
              },
              {
                lable: "1/3 Investigator – Martyr",
              },
              {
                lable: "1/3 Investigator – Martyr",
              },
              {
                lable: "1/3 Investigator – Martyr",
              },
            ],
            [
              {
                lable: "4/6 Opportunist - Role Model",
              },
              {
                lable: "4/6 Opportunist - Role Model",
              },
              {
                lable: "4/6 Opportunist - Role Model",
              },
              {
                lable: "4/6 Opportunist - Role Model",
              },
              {
                lable: "4/6 Opportunist - Role Model",
              },
              {
                lable: "4/6 Opportunist - Role Model",
              },
            ],
          ]}
        />
        <div>
          <SectionFooter />
        </div>
      </div>
    </>
  );
};

export default Page;
