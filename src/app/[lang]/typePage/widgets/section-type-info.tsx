
import { Button } from '@/shared/ui'
import './section-type-info.scss'

type TSectionTypeInfo = {
  infoList: TInfo[]
}

type TInfo = {
  title: string,
  body: Array<{
    title?: string,
    paragraph: string,
  }>,
}

const infoList: TInfo[] = [
  {
    title: 'The Manifesting Generator’s Life Strategy',
    body: [
      {
        paragraph: 'Responding to the opportunities that come your way is the ideal strategy for you; it’s similar to the Generators’ strategy. But how can a person with such powerful internal energy simply wait around for opportunities to just come about, when he is “the one who knocks”? The Manifesting Generator receives internal feedback when he or she has already started doing something; he/she first gets a “taste” of it and only then understands whether it’s worth it to keep going or not. However, an incredibly decisive beginning can be replaced by passionate rejection. Your strong point lies in the absence of energy fluctuations, which usually hinder classic Generators. The most important lesson that Manifesting Generator needs to learn is to have patience and tolerance for people. By being able to hold yourself back and refraining from always doing everything “right here and now,” you will allow life itself to bring you wonderful surprises. Sometimes you need to let life take shape without your active participation.',
      },
    ],
  },
  {
    title: 'Obstructions in the Manifesting Generator’s Path',
    body: [
      {
        paragraph: 'The creative energy within you has been in sixth gear since early childhood and doesn’t take kindly to restrictions. However, if you ignore your internal feedback, you will encounter a problem that is common for the Manifestor: irritation and anger. You may find yourself struggling with the questions of why everything is so slow and why everyone else is not as determined or strong-willed as you. Manifesting Generators can often develop problems with their thyroid gland as a result of their constant lack of patience and burning desire to receive instant results and gratification.'
      },
      {
        title: 'The Manifesting Generator’s Work',
        paragraph: 'It is not typical for your type to polish your completed work down to the smallest detail or to bring something to absolute perfection. At the same time, the Manifesting Generator doesn’t tolerate criticism well, either. Any proposal to re-do something will be met with your outrage and eventually your desire to drop the work altogether. Your loved ones constantly seek your immeasurable supplies of energy and efficiency, but you sometimes need to direct them both in the proper direction: to fulfill your own needs and achieve your own satisfaction.'
      },
    ]
  }
]

const SectionTypeInfo = ({ }: TSectionTypeInfo) => {
  return (
    <div
      className="section section-type-info"
    >
      <div className="divider" />
      <div
        className="section-type-info-list"
      >
        {infoList.map((item, index) => (
          <div
            className='section-type-info-list--item'
            key={index}
          >
            <h1
              className='section-type-info-list--title'
            >
              {item.title}
            </h1>
            <div
              className='section-type-info-list--body'
            >
              {item.body.map((item, index) => (
                <>
                  {item.title && (
                    <h3
                      className='section-type-info-list--point'
                    >
                      {item.title}
                    </h3>
                  )}
                  <p
                    className='section-type-info-list--paragraph text-landing text-gray'
                  >
                    {item.paragraph}
                  </p>
                </>
              ))}
              {index === infoList.length - 1 && (
                <div className='section-type-info--button-wrapper'>
                  <Button>
                    Start
                  </Button>
                  <p className='text description-text-color text-description'>
                    Calculate a personal <br />
                    chart for Free
                  </p>
                </div>
              )}
            </div>
          </div>
        ))}
      </div>
      <div className="divider" />
    </div>
  );
};

export default SectionTypeInfo
