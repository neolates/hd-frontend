import defaultAvatart from "@assets/avatar.png";
import UserAvatar from "@/widgets/UserAvatar";
import { Button } from "@/shared/ui";

import "./discussion-card.scss";

type TDiscussionCard = {};

const DiscussionCard = ({}: TDiscussionCard) => {
  return (
    <div className="discussion-card">
      <div className="discussion-card--more-options"></div>
      <div className="discussion-card--header">
        <UserAvatar height={50} width={50} src={defaultAvatart} alt={""} style={{}} />
        <div className="discussion-card--user-info">
          <Button>
            <p>Bobby Sid</p>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="13"
              height="12"
              viewBox="0 0 13 12"
              fill="none">
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M2.27836 7.12542L0.441402 5.7317C0.0541687 5.43727 -0.0946441 4.95083 0.0605694 4.48999C0.215783 4.02915 0.628619 3.73312 1.11506 3.73312H3.38726C3.9121 3.71872 4.35214 3.36989 4.50416 2.89145L5.19381 0.769667C5.34423 0.302426 5.76186 0 6.25151 0C6.74115 0 7.15879 0.302426 7.3092 0.769667L7.99886 2.89145C8.15087 3.36989 8.59091 3.71872 9.11576 3.73312H11.388C11.8744 3.73312 12.2872 4.02915 12.4424 4.48999C12.5977 4.95083 12.4488 5.43727 12.0616 5.7317L10.2247 7.12542H10.2263C9.93663 7.34464 9.74941 7.69347 9.74941 8.0855C9.74941 8.21511 9.77021 8.33992 9.80702 8.45673L10.4839 10.5401C10.6343 11.001 10.4823 11.481 10.095 11.7722C9.70941 12.0634 9.20536 12.0762 8.80373 11.8058L6.91716 10.5337C6.72675 10.4073 6.49793 10.3337 6.25151 10.3337C6.00509 10.3337 5.77627 10.4073 5.58585 10.5337L3.69929 11.8058C3.29765 12.0762 2.79361 12.0634 2.40797 11.7722C2.02074 11.481 1.86873 11.001 2.01914 10.5401L2.696 8.45673C2.7328 8.33992 2.7536 8.21511 2.7536 8.0855C2.7536 7.69347 2.56639 7.34464 2.27676 7.12542H2.27836Z"
                fill="url(#paint0_linear_2503_22584)"
              />
              <defs>
                <linearGradient
                  id="paint0_linear_2503_22584"
                  x1="6.25151"
                  y1="0"
                  x2="6.25151"
                  y2="12"
                  gradientUnits="userSpaceOnUse">
                  <stop stop-color="#FFE8DB" />
                  <stop offset="1" stop-color="#9E9E9E" />
                </linearGradient>
              </defs>
            </svg>
          </Button>
          <p className="text-description text-gray">4m ego</p>
        </div>
      </div>
      <div className="discussion-card--body">
        <h4>Talking to you is always a pleasure.</h4>
        <p className="discussion-card--text">
          Talking to you is always a pleasure. You are a thoughtful friend, a great human behaviour
          expert, and an expert on how the energy of life works. You do not possess an innate source
          of energy, yet communicating with other people you pass a vast amount of energy through
          yourself. Learning the right way to get access to and to use this energy will reward you
          with something you need so much - recognition and appreciation of others.
        </p>
      </div>

      <div className="discussion-card--footer">
        <Button className="discussion-card--button">Discussion</Button>
        <div className="discussion-card--readers">
          <UserAvatar height={40} width={40} src={defaultAvatart} alt={""} style={{}} />
          <UserAvatar height={40} width={40} src={defaultAvatart} alt={""} style={{}} />
          <UserAvatar height={40} width={40} src={defaultAvatart} alt={""} style={{}} />
          <UserAvatar height={40} width={40} src={defaultAvatart} alt={""} style={{}} />
          <UserAvatar height={40} width={40} src={defaultAvatart} alt={""} style={{}} />
          <Button className="discussion-card--readers-number">12.5K</Button>
        </div>
        <Button className="discussion-card--likes">
          <p>5.5K</p>
          <div>&hearts;</div>
        </Button>
      </div>
    </div>
  );
};

export default DiscussionCard;
