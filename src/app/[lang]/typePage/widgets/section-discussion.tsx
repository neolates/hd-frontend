"use client";

import { Navigation } from "swiper";
import { Swiper as SwiperComponent, SwiperSlide } from "swiper/react";
import { Button } from "@/shared/ui";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "./section-discussion.scss";
import DiscussionCard from "./discussion-card";

type TSectionDiscussion = {
  discussionList: TDiscussion[];
};

type TDiscussion = {
  title: string;
};

const SectionDiscussion = ({ discussionList }: TSectionDiscussion) => {
  return (
    <div className="section section-discussion">
      <div className="section-discussion--header">
        <h1>Discussions</h1>
        <div className="section-discussion--controllers">
          <Button className="section-discussion--prev">
            <div>
              <svg xmlns="http://www.w3.org/2000/svg" width="6" height="8" viewBox="0 0 6 8">
                <path
                  d="M3.78068 0.294814C4.1759 -0.0982715 4.78854 -0.0982715 5.1839 0.294814C5.57911 0.6879 5.57911 1.29724 5.1839 1.69046L3.8598 3.00742L3 3.99015L3.8598 4.97302L5.20359 6.30957C5.5988 6.70265 5.5988 7.31199 5.20359 7.70522C5.0059 7.90183 4.74908 8 4.51183 8C4.25487 8 3.99803 7.90169 3.82006 7.70522L0.796383 4.69784C0.618551 4.52097 0.5 4.26541 0.5 4.00981C0.5 3.75421 0.598842 3.49879 0.796383 3.32178L3.78068 0.294814Z"
                  fill="#1A1A1A"
                />
              </svg>
            </div>
          </Button>
          <Button className="section-discussion--next">
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="6"
                height="8"
                viewBox="0 0 6 8"
                fill="none">
                <path
                  d="M2.21932 0.294814C1.8241 -0.0982715 1.21146 -0.0982715 0.816105 0.294814C0.420888 0.6879 0.420888 1.29724 0.816105 1.69046L2.1402 3.00742L3 3.99015L2.1402 4.97302L0.796413 6.30957C0.401196 6.70265 0.401196 7.31199 0.796413 7.70522C0.994095 7.90183 1.25092 8 1.48817 8C1.74513 8 2.00197 7.90169 2.17994 7.70522L5.20362 4.69784C5.38145 4.52097 5.5 4.26541 5.5 4.00981C5.5 3.75421 5.40116 3.49879 5.20362 3.32178L2.21932 0.294814Z"
                  fill="#1A1A1A"
                />
              </svg>
            </div>
          </Button>
        </div>
      </div>
      <div className="section-discussion--body">
        <SwiperComponent
          breakpoints={{
            1300: {
              slidesPerView: 2,
            },
          }}
          navigation={{
            nextEl: ".section-discussion--next",
            prevEl: ".section-discussion--prev",
          }}
          grabCursor={false}
          modules={[Navigation]}
          spaceBetween={20}
          slidesPerView={1}
          loop={true}
          className="discussion-swiper">
          {discussionList.map((item, index) => (
            <SwiperSlide key={index}>
              <DiscussionCard />
            </SwiperSlide>
          ))}
        </SwiperComponent>
      </div>
    </div>
  );
};

export default SectionDiscussion;
