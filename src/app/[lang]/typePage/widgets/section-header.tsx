import Image, { StaticImageData } from "next/image";
import { Button } from "@/shared/ui";

import "./section-header.scss";

type TSectionHeader = {
  subtitle: string;
  title: string;
  text: string;
  img: StaticImageData;
};

const SectionHeader = ({ img, subtitle, title, text }: TSectionHeader) => {
  return (
    <div className="section section-header">
      <div className="section-header-content text-white">
        <div className="section-header-headline">
          <p className="section-header-headline__subtitle">{subtitle}</p>
          <h1 className="section-header-headline__title">{title}</h1>
        </div>

        <div className="section-header-text mt-3">
          <p className="text text-landing text-gray section-header__text">{text}</p>
          <div className="btncontainer">
            <Button className="btncontainer__btn" variant="default">
              Start
            </Button>
            <h4 className="btncontainer__desc text-description text-gray-dark">
              Calculate a personal
              <br />
              chart for Free
            </h4>
          </div>
        </div>
      </div>

      <div className="section-header-picture">
        <Image src={img} alt=""></Image>
      </div>
    </div>
  );
};

export default SectionHeader;
