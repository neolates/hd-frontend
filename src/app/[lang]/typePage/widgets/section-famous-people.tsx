import { StaticImageData } from "next/image";

import avatar from "@assets/avatar.png";
import UserAvatar from "@/widgets/UserAvatar";
import { SliderX, SliderXItem } from "@/shared/ui";

import "./section-famous-people.scss";

type TSectionFamousPeople = {
  peopleList: TFamousPerson[];
};

type TFamousPerson = {
  img: StaticImageData;
};

const famousPeople: TFamousPerson[] = [
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
  {
    img: avatar,
  },
];

const SectionFamousPeople = ({ peopleList }: TSectionFamousPeople) => {
  return (
    <div className="section-famous-people section">
      <div className="divider"></div>
      <div className="section-famous-people--content-wrapper">
        <h1 className="section-famous-people--title">
          Famous people among Manifesting Generator’s
        </h1>
        <SliderX
          className="section-famous-people--slider "
          firstGapClassName="section-famous-people--slider-gap"
          lastGapClassName="section-famous-people--slider-gap">
          {famousPeople.map((item, index) => (
            <SliderXItem key={index}>
              <UserAvatar className="famous-person" alt="famous person" src={item.img} style={{}} />
            </SliderXItem>
          ))}
        </SliderX>
      </div>
    </div>
  );
};

export default SectionFamousPeople;
