import BaseLayout from "@/widgets/layouts/BaseLayout";

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return <BaseLayout>{children}</BaseLayout>;
}
