"use client";

import Image from "next/image";

import React, { ReactElement } from "react";

import "./bodygraph.scss";

import StatusDoughnut from "@/widgets/StatusDoughnut";
import UserAvatar from "@/widgets/UserAvatar";

import BodyGraph from "@/features/bodygraph/bodygraph";
import BodyGraphCard from "@/features/bodygraph/bodygraph-card";

import { Button } from "@/shared/ui";

/* eslint-disable */
import user1 from "./assets/user1.png";
import user2 from "./assets/user2.png";
import user3 from "./assets/user3.png";
import user4 from "./assets/user4.png";
import facts from "./assets/facts.svg";
import childChart from "./assets/child-chart.svg";
import foreCast from "./assets/forecast.svg";
import spheres from "./assets/spheres.svg";
import dreamTeam from "./assets/dream-team.svg";
import compatability from "./assets/compatability.svg";
import projector from "./assets/projector.svg";
/* eslint-enable */

export default function bodygraph(): ReactElement {
  const cards = [
    {
      text: "Facts About You",
      icon: facts,
      status: "Free",
      btnTitle: "Read",
      className: "bodygraph-card-facts",
    },
    {
      text: "Child Chart",
      icon: childChart,
      status: "Purchase",
      btnTitle: "Culculate",
      className: "bodygraph-card-child-chart",
    },
    {
      text: "Forecasts",
      icon: foreCast,
      status: "Purchase",
      btnTitle: "Open",
      className: "bodygraph-card-forecast",
    },
    {
      text: "Spheres of life",
      icon: spheres,
      status: "Purchase",
      btnTitle: "Read",
      className: "bodygraph-card-spheres",
    },
    {
      text: "Dream Team",
      icon: dreamTeam,
      status: "Purchase",
      btnTitle: "Read",
      className: "bodygraph-card-dreamTeam",
    },
    {
      text: "Compatibility",
      icon: compatability,
      status: "Purchase",
      btnTitle: "Culculate",
      className: "bodygraph-card-compatability",
    },
  ];

  return (
    <>
      <div className="py-5 px-3 gap-10 lg:gap-0 flex flex-wrap bodygraph-bg">
        <div className="w-full lg:w-1/2 z-10">
          <Image src={projector} alt="indentify" className="img-bodygraph mb-3" />
          <p className="date-bodygraph">Bobby Sid, 06.02.1990 06:20</p>
          <div className="flex">
            <h1 className="font-semibold mb-3 main-title-bodygraph">Projector</h1>
            <span className="question-mark"></span>
          </div>
          <div className="flex mb-3">
            <h4 className="font-semibold second-title-bodygraph">
              Profile 5/1 Heretic – Investigator
            </h4>
            <span className="question-mark"></span>
          </div>
          <p className="font-normal paragraph-bodygraph leading-[28px] mb-4">
            You are as a bright personality, as a profound one. You fly by the seat of pants,
            overcome obstacles with grace and try to get to the bottom of things. Creativity,
            commitment and solid knowledge – these are three pillars supporting the basis of your
            reputation in the society. You are a born leader, tutor and guide. You generously share
            your experience and wisdom with others. The only thing beyond your understanding is
            yourself.
          </p>
        </div>
        <div className="w-full lg:w-1/2 flex relative z-10 lg:mb-7 order-2 sm:order-1">
          <BodyGraph />
        </div>
        <div className="bodygraph-actions order-1 flex flex-wrap justify-between w-full items-center z-10 gap-7 md:gap-0">
          <div className="flex flex-wrap lg:flex-nowrap gap-2 w-full md:w-1/2 order-2 sm:-order-none">
            <div className="w-full md:w-auto">
              <Button>Personal Reading</Button>
            </div>
            <div className="w-full md:w-auto">
              <Button variant="outline">Invite Friend</Button>
            </div>
          </div>
          <div className="flex flex-wrap lg:flex-nowrap xl:justify-end gap-7 xl:gap-0 w-full md:w-1/2">
            <div className="flex w-full lg:w-auto md:justify-end py-2 lg:py-0 border lg:border-0 border-solid border-gray-500 border-opacity-50 border-collapse border-r-0 border-l-0">
              <div className="doughnut mr-2">
                <StatusDoughnut />
              </div>
              <div className="stats-doughnut">
                <div className="description-text-color font-medium text-md mb-2">
                  People of your type
                </div>
                <div className="font-semibold text-xxl leading-40">2.8k</div>
              </div>
            </div>
            <hr className="bodygraph-vertical-line mx-3 hidden lg:block" />
            <div className="flex flex-col w-full lg:w-auto md:items-end pb-2 lg:pb-0 border lg:border-0 border-solid border-gray-500 border-opacity-50 border-collapse border-r-0 border-l-0 border-t-0">
              <div className="description-text-color font-medium text-md mb-2">Latest members</div>
              <div className="avatars flex items-center relative">
                {[user1, user2, user3, user4].map((user, index) => (
                  <UserAvatar
                    width={40}
                    height={40}
                    src={user}
                    alt={`user-avatar-${index}`}
                    className="relative"
                    key={index}
                    style={{ left: `-${index * 10}px` }}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bodygraph-cards flex flex-wrap py-2 gap-1 sm:gap-5 justify-between sm:justify-start px-2 sm:px-0">
        {cards.map(({ text, icon, status, btnTitle, className }, index) => (
          <BodyGraphCard
            key={index}
            text={text}
            icon={icon}
            status={status}
            btnTitle={btnTitle}
            className={className}
          />
        ))}
      </div>
    </>
  );
}
