import { RefObject, useEffect } from "react";

export function useOutsideClick(elRef: RefObject<HTMLElement>, handler: () => void) {
  useEffect(() => {
    const outsideClickHandler = (e: MouseEvent) => {
      if (elRef.current && !elRef.current.contains(e.target as Node) && handler) handler();
    };
    window.document.addEventListener("click", outsideClickHandler);
    return () => {
      window.document.removeEventListener("click", outsideClickHandler);
    };
  }, [handler, elRef]);
}
