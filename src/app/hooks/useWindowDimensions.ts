"use client";

import { useCallback, useEffect, useState } from "react";

export const useWindowDimensions = () => {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);
  const getWindowDimensions = useCallback(() => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  }, []);
  useEffect(() => {
    if (document && window) {
      document.addEventListener("resize", getWindowDimensions);
      if (width === 0 && height === 0) getWindowDimensions();
      return () => document.removeEventListener("resize", getWindowDimensions);
    }
  });
  return {
    width,
    height,
  };
};
