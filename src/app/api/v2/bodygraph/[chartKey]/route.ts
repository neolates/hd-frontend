import moment from "moment-timezone";

import { NextResponse } from "next/server";

import qs from "qs";

import { ChartKey } from "@/entities";

import { base64Url } from "@/shared/lib";

// query example: year=1954&month=0&placeId=153209&day=12&hours=12&minutes=00
export async function GET(request: Request, { params }: { params: { chartKey: string } }) {
  const queryParams = ChartKey.parse(base64Url.decode(params.chartKey));

  const year = parseInt(queryParams.year);
  const month = parseInt(queryParams.month);
  const day = parseInt(queryParams.day);
  const hours = parseInt(queryParams.hours);
  const minutes = parseInt(queryParams.minutes);

  const dateParts = [year, month, day, hours, minutes];
  if (dateParts.some((v) => isNaN(v))) {
    return NextResponse.json({ data: null, error: "query error" }, { status: 400 });
  }

  const placeId = queryParams.placeId;
  let url = `https://geoapi.humandesignx.com/api/v1/places/details?placeid=${placeId}&language=ru`;
  const geo = await fetch(url, { cache: "default" }).then((res) => res.json());

  const date = moment.tz(dateParts, geo.result.timezone).format("YYYY-MM-DDTHH:mmZZ");
  url = `https://api.humandesignx.com/api/v1/hd/bodygraph?${qs.stringify({ date })}`;
  const data = await fetch(url, { cache: "default" }).then((res) => res.json());

  return NextResponse.json({ data });
}
