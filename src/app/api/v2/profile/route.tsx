export const dynamic = "force-dynamic";

import { PrismaClient } from "@prisma/client";

import { Session, createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";

import jwt from "jsonwebtoken";

import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { applySupabaseRLS } from "@/shared/lib";

const prisma = new PrismaClient();

const getAuthendPrisma = (client: PrismaClient, session: Session) => {
  const claims = jwt.decode(session.access_token);
  return client
    .$extends(
      applySupabaseRLS({
        name: "prisma-extension-rls-jwt-claims",
        claimsSetting: "request.jwt.claims",
        claimsFn: () => claims,
      })
    )
    .$extends(
      applySupabaseRLS({
        name: "prisma-extension-rls-jwt-claim-sub",
        claimsSetting: "request.jwt.claim.sub",
        claimsFn: () => claims.sub,
      })
    )
    .$extends(
      applySupabaseRLS({
        name: "prisma-extension-rls-jwt-claims-role",
        claimsSetting: "request.jwt.claim.role",
        claimsFn: () => claims.role,
      })
    )
    .$extends(
      applySupabaseRLS({
        name: "prisma-extension-rls-jwt-claim-email",
        claimsSetting: "request.jwt.claim.email",
        claimsFn: () => claims.email,
      })
    );
};

export async function GET() {
  const supabase = createRouteHandlerClient({ cookies });

  const session = await supabase.auth.getSession().then((session) => session.data.session);
  if (!session) {
    return;
  }

  const data = await getAuthendPrisma(prisma, session).userProfile.findMany();

  const supabaseClientData = await supabase.from("userProfile").select();
  return NextResponse.json({ jwt, data, supabaseClientData });
}
