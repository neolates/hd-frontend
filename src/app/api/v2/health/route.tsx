import { NextResponse } from "next/server";

import { http } from "@/shared/lib";

export async function GET() {
  return NextResponse.json({ baseUrl: http.getBaseApiUrl() });
}
