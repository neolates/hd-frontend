import { PrismaClient } from "@prisma/client";
import { NextRequest, NextResponse } from "next/server";

const prisma = new PrismaClient();

export async function GET(
  request: Request,
  { params }: { params: { lang: string; editionKey: string; contentKey: string } }
) {
  try {
    const data = await prisma.readingPartContent.findUnique({
      where: {
        lang_editionKey_contentKey: {
          lang: params.lang,
          editionKey: params.editionKey,
          contentKey: params.contentKey,
        },
      },
    });
    // console.log(data);
    return NextResponse.json({ data });
  } catch (e) {
    // console.log(e);
    return NextResponse.json({ data: null, error: "item not found" }, { status: 404 });
  }
}

export interface ReadingPartContentUpdateInput {
  title?: string;
  article?: string;
}

export async function PUT(
  request: NextRequest,
  { params }: { params: { lang: string; editionKey: string; contentKey: string } }
) {
  const data = (await request.json()) as ReadingPartContentUpdateInput;

  await prisma.readingPartContent.update({
    where: {
      lang_editionKey_contentKey: {
        lang: params.lang,
        editionKey: params.editionKey,
        contentKey: params.contentKey,
      },
    },
    data,
  });

  return NextResponse.json({});
}
