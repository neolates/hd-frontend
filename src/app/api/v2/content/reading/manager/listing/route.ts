import { PrismaClient } from "@prisma/client";

import { NextResponse } from "next/server";

import {
  EditionKeysGenerator,
  ReadingEditionType,
  ReadingManagerListingData,
  ReadingSection,
  ReadingSections,
} from "@/entities";

const prisma = new PrismaClient();

// curl -X POST 'http://localhost:3000/api/v2/content/reading/manager/produce' -d '{"editionType": "default", "editionKey": "standard", "langs": ["ru", "en", "id"]}'

class ReadingSectionResolver {
  cache: Map<string, ReadingSection>;
  generators: Map<ReadingEditionType, EditionKeysGenerator>;

  private makeCacheKey(editionType: ReadingEditionType, contentKey: string): string {
    return `${editionType}_${contentKey}`;
  }

  constructor() {
    this.cache = new Map();
    this.generators = new Map();
  }

  Resolve(editionType: ReadingEditionType, contentKey: string): ReadingSection | undefined {
    const cacheKey = this.makeCacheKey(editionType, contentKey);

    let resultSectionKey = this.cache.get(cacheKey);

    if (resultSectionKey) {
      // Достали из кэша
      return resultSectionKey;
    }

    let generator = this.generators.get(editionType);
    if (!generator) {
      generator = new EditionKeysGenerator(editionType);
      this.generators.set(editionType, generator);
    }

    for (const generatorSectionKey of ReadingSections) {
      const contentKeys = generator.generateContentKeys(generatorSectionKey);
      contentKeys
        .map((contentKey) => this.makeCacheKey(editionType, contentKey))
        .forEach((key) => {
          this.cache.set(key, generatorSectionKey);
        });

      resultSectionKey = this.cache.get(cacheKey);
      if (resultSectionKey) {
        // Достали из кэша
        return resultSectionKey;
      }
    }
  }
}

// curl 'http://localhost:3000/api/v2/content/reading/manager/listing'

export async function GET() {
  const readingSectionResolver = new ReadingSectionResolver();
  const data: ReadingManagerListingData = await prisma.readingPartContent
    .findMany({
      select: {
        lang: true,
        editionType: true,
        editionKey: true,
        contentKey: true,
        title: true,
      },
    })
    .then((items) =>
      items.reduce((editions, item) => {
        const sectionKey = readingSectionResolver.Resolve(
          item.editionType as ReadingEditionType,
          item.contentKey
        );

        if (!sectionKey) {
          // Something wrong!
          return editions;
        }

        const editionsOfType = editions[item.editionType] ?? {};
        const edition = editionsOfType[item.editionKey] ?? {};
        const section = edition[sectionKey] ?? {};
        const content = section[item.contentKey] || {
          langs: [],
          titles: {},
        };
        content.langs.push(item.lang);
        content.titles[item.lang] = item.title || item.contentKey;

        section[item.contentKey] = content;
        edition[sectionKey] = section;
        editionsOfType[item.editionKey] = edition;
        editions[item.editionType] = editionsOfType;
        return editions;
      }, {} as ReadingManagerListingData)
    );

  return NextResponse.json({ data });
}
