import { PrismaClient } from "@prisma/client";
import { NextRequest, NextResponse } from "next/server";

const prisma = new PrismaClient();

export interface ReadingPartContentUpdateInput {
  title?: string;
  article?: string;
}

// curl -X POST 'http://localhost:3000/api/v2/content/reading/manager/produce' -d '{"editionType": "default", "editionKey": "standard", "langs": ["ru", "en", "id"]}'
// curl -X POST 'http://localhost:3000/api/v2/content/reading/manager/produce' -d '{"editionType": "default", "editionKey": "standard", "langs": ["id"]}'

export async function POST(
  request: NextRequest,
  { params }: { params: { lang: string; editionKey: string; contentKey: string } }
) {
  const data = (await request.json()) as ReadingPartContentUpdateInput;

  await prisma.readingPartContent.update({
    where: {
      lang_editionKey_contentKey: {
        lang: params.lang,
        editionKey: params.editionKey,
        contentKey: params.contentKey,
      },
    },
    data,
  });

  return NextResponse.json({});
}
