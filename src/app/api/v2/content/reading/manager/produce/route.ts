import { PrismaClient } from "@prisma/client";
import { revalidateTag } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

import { ReadingContentManager, ReadingEditionType } from "@/entities";

const prisma = new PrismaClient();

interface ProduceReadingEdition {
  editionType: ReadingEditionType;
  editionKey: string;
  langs: string[];
}

// curl -X POST 'http://localhost:3000/api/v2/content/reading/manager/produce' -d '{"editionType": "default", "editionKey": "standard", "langs": ["ru", "en", "id"]}'
// curl -X POST 'http://localhost:3000/api/v2/content/reading/manager/produce' -d '{"editionType": "default", "editionKey": "standard", "langs": ["id"]}'

export async function POST(request: NextRequest) {
  revalidateTag("reading-manager-listing");

  const params = (await request.json()) as ProduceReadingEdition;

  const manager = new ReadingContentManager();
  const data = manager.ProduceNewReadingContent(params.editionType, {
    langs: params.langs,
    editionKey: params.editionKey,
  });

  await prisma.readingPartContent.createMany({
    data,
  });

  return NextResponse.json({});
}
