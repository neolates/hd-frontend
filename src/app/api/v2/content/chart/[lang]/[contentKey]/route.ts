import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";

const prisma = new PrismaClient();

export async function GET(
  request: Request,
  { params }: { params: { lang: string; contentKey: string } }
) {
  try {
    const data = await prisma.chartContent.findUniqueOrThrow({
      where: {
        lang_contentKey: {
          lang: params.lang,
          contentKey: params.contentKey,
        },
      },
    });
    return NextResponse.json({ data });
  } catch (e) {
    return NextResponse.json({ data: null, error: "item not found" }, { status: 404 });
  }
}
