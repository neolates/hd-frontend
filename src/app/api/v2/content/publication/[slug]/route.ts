import { PrismaClient } from "@prisma/client";
import { NextResponse } from "next/server";

const prisma = new PrismaClient();

export async function GET(request: Request, { params }: { params: { slug: string } }) {
  try {
    const data = await prisma.publicationContent.findUniqueOrThrow({
      where: {
        slug: params.slug,
      },
    });
    return NextResponse.json({ data });
  } catch (e) {
    // console.log(e);
    return NextResponse.json({ data: null, error: "publication not found" }, { status: 404 });
  }
}
