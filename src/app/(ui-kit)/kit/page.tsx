"use client";

import React, { ReactElement, useEffect, useMemo, useState } from "react";

import {
  AutocompleteField,
  Button,
  ButtonSize,
  ButtonVariant,
  SelectField,
  SelectOptionItem,
  TextField,
} from "@/shared/ui";

function Page(): ReactElement {
  const [search, setSearch] = useState("");

  const options: SelectOptionItem[] = useMemo(
    () => [
      { value: 1, text: "Apple" },
      { value: 2, text: "Tomato" },
      { value: 3, text: "Banana" },
      { value: 4, text: "Avacado" },
      { value: 5, text: "Avacado" },
      { value: 6, text: "Car" },
      { value: 7, text: "House" },
      { value: 8, text: "Phone" },
      { value: 9, text: "Foo" },
      { value: 10, text: "Bar" },
    ],
    []
  );

  const [filteredOptions, setFilteredOptions] = useState([...options]);

  useEffect(() => {
    setFilteredOptions(() =>
      search
        ? options.filter((option) =>
            option.text.toString().toLowerCase().startsWith(search.toLowerCase())
          )
        : options
    );
  }, [search, options]);

  return (
    <>
      <div className="flex gap-8">
        {(
          ["outline", "default", "text", "service", "service-light", "icon"] as ButtonVariant[]
        ).map((variant) => (
          <div key={`${variant}`}>
            {(["sm", "md"] as ButtonSize[]).map((size) => (
              <div key={`${variant}-${size}`}>
                <Button size={size} variant={variant} href="/path">
                  {size} {variant}
                </Button>
                <Button
                  size={size}
                  variant={variant}
                  onClick={() => console.log("Button clicked!")}>
                  {size} {variant}
                </Button>
              </div>
            ))}
          </div>
        ))}
      </div>
      <div className="flex gap-8">
        <TextField name="text-field" placeholder="Enter your name"></TextField>
        <SelectField options={options} name="select-field"></SelectField>
        <AutocompleteField
          options={filteredOptions}
          name="autocomplete-field"
          placeholder="Enter Birth City"
          onSearch={async (v) => {
            await new Promise((r) => setTimeout(() => r(true), 1000));
            setSearch(() => v);
          }}
          emptyListElement={<>No city found!</>}
          variant={"value-right"}
        />
      </div>
    </>
  );
}

export default Page;
