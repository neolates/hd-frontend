import { NextRequest } from "next/server";
import { i18nMiddleware } from "./shared/middleware/i18n";

export function middleware(request: NextRequest) {
  return i18nMiddleware(request);
}
