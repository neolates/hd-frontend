import { ReactNode } from "react";
import "./SliderX.scss";

interface Props {
  children: ReactNode;
}

export const SliderXItem: React.FC<Props> = ({ children }) => {
  return <div className="slider-x-item">{children}</div>;
};
