import clsx from "clsx";
import { ReactNode } from "react";
import "./SliderX.scss";

interface Props {
  children: ReactNode;
  className?: string;
  firstGapClassName?: string;
  lastGapClassName?: string;
}

export const SliderX: React.FC<Props> = ({
  children,
  className,
  firstGapClassName,
  lastGapClassName,
}) => {
  return (
    <div className={clsx(className, "slider-x scrollbar-hide")}>
      <div className={clsx(firstGapClassName)}></div>
      {children}
      <div className={clsx(lastGapClassName)}></div>
    </div>
  );
};
