"use client";

import clsx from "clsx";

import { FC } from "react";

import ButtonBase from "./ButtonBase";

import { ButtonBaseProps, ButtonSize, ButtonVariant } from "./types";

import "./Button.scss";

type ButtonProps = ButtonBaseProps & {
  variant?: ButtonVariant;
  size?: ButtonSize;
};

const Button: FC<ButtonProps> = ({ size = "md", variant = "default", ...props }) => {
  const _className = clsx(
    props.className,
    "button",
    `button-variant--${variant}`,
    `button-size--${size}`
  );
  return <ButtonBase {...props} className={clsx(_className, props.className)}></ButtonBase>;
};

export default Button;
