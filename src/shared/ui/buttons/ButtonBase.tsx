"use client";

import Link from "next/link";

import React, { FC } from "react";

import { ButtonBaseProps } from "./types";

const ButtonBase: FC<ButtonBaseProps> = (props) => {
  if ("href" in props) {
    // We are dealing with a Link
    const { href, children, ...restProps } = props;
    return (
      <div>
        <Link href={href} {...restProps}>
          {children}
        </Link>
      </div>
    );
  } else {
    // We are dealing with a Button
    return (
      <div>
        <button {...props}></button>
      </div>
    );
  }
};

export default ButtonBase;
