import { ButtonHTMLAttributes, DetailedHTMLProps } from "react";

type ButtonInternalProps = DetailedHTMLProps<
  ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>;
type LinkInternalProps = { href: string; [key: string]: any };

export type ButtonBaseProps = ButtonInternalProps | LinkInternalProps;
export type ButtonSize = "sm" | "md";
export type ButtonVariant = "outline" | "text" | "service" | "service-light" | "default" | "icon";
