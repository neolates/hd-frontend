"use client";

import { MDXEditor } from "@mdxeditor/editor";

import clsx from "clsx";

import { ChangeEvent, useState } from "react";

import "@mdxeditor/editor/style.css";

import "./MarkdownField.scss";

export interface MarkdownFieldProps {
  value?: string;
  name: string;
  type?: "text";
  placeholder?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  className?: string;
  forwardedRef?: React.ForwardedRef<any>;
}

export const MarkdownField = (props: MarkdownFieldProps) => {
  const [isInitialized, setIsInitialized] = useState(false);

  const onChangeHandle = (v: string) => {
    if (!isInitialized) {
      setIsInitialized(true);
      return;
    }

    if (props.onChange) {
      // console.log(v);
      const event: ChangeEvent<HTMLInputElement> = {
        target: {
          name: props.name,
          value: v,
        },
      } as any;
      props.onChange(event);
    }
  };

  return (
    <>
      <input
        type="hidden"
        name={props.name}
        value={props.value}
        ref={props.forwardedRef}
        readOnly
      />
      <MDXEditor
        className={clsx("mdx-editor-wrapper", props.className ?? "")}
        markdown={props.value ?? ""}
        onChange={onChangeHandle}
        contentEditableClassName="mdx-editor-content"
      />
    </>
  );
};

export default MarkdownField;
