export type PropsWithFieldVariant<P = unknown> = P & { variant?: FieldVariant | undefined };

export type PropsWithLabel<P = unknown> = P & { label?: string | undefined };

export type PropsWithOptions<P = unknown> = P & { options: SelectOptionItem[] | undefined };

export type FieldVariant = "value-right" | "value-left";

export interface SelectOptionItem {
  value: string | number;
  text: string | number;
}
