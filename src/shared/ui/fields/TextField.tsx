import clsx from "clsx";

import { DetailedHTMLProps, InputHTMLAttributes, useId } from "react";

import { PropsWithFieldVariant, PropsWithLabel } from "./types";

import "./TextField.scss";

export type TextFieldProps = PropsWithFieldVariant<
  PropsWithLabel<DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>>
> & { invalid?: boolean };

const TextField = ({ variant, invalid, ...props }: TextFieldProps) => {
  let id = useId();
  if (props.id) {
    id = props.id;
  }
  return (
    <div
      className={clsx("input-field", {
        "value-right": variant === "value-right",
        invalid,
      })}>
      {/* use variant for component styling */}
      <label htmlFor={id} className="input-field__label">
        {props.label}
      </label>
      <input {...props} id={id} className="input-field__input" />
    </div>
  );
};

export { TextField };
