import { SelectHTMLAttributes, useId } from "react";
import { PropsWithFieldVariant, PropsWithLabel, PropsWithOptions } from "./types";

export type SelectFieldProps = PropsWithFieldVariant<
  PropsWithOptions<PropsWithLabel<SelectHTMLAttributes<HTMLSelectElement>>>
>;

export const SelectField = ({ variant, label, options, ...selectProps }: SelectFieldProps) => {
  let id = useId();
  if (selectProps.id) {
    id = selectProps.id;
  }
  // console.log(variant);
  return (
    <>
      {/* use variant for component styling */}
      {label && <label htmlFor="{id}">{label}</label>}
      <select {...selectProps} id={id}>
        {options &&
          options.map((item) => (
            <option key={item.value} value={item.value}>
              {item.text}
            </option>
          ))}
      </select>
    </>
  );
};
