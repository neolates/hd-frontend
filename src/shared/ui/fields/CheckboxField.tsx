import Image from "next/image";
import { InputHTMLAttributes, PropsWithChildren, useId } from "react";
import DoneIcon from "@assets/done.svg";
import { PropsWithLabel } from "./types";

import "./CheckboxField.scss";

export type CheckboxFieldProps = PropsWithLabel<
  PropsWithChildren<InputHTMLAttributes<HTMLInputElement>>
>;

export const CheckboxField = ({ children, id, label, ...props }: CheckboxFieldProps) => {
  let inputId = useId();
  if (id) {
    inputId = id;
  }
  return (
    <div className="checkbox-field ">
      <input {...props} type="checkbox" id={inputId} className="hidden checkbox-field__input" />
      <div className="checkbox-field__title">
        <label htmlFor={inputId} className="checkbox-field__label">
          <div className="checkbox-field__checkbox">
            <Image src={DoneIcon} alt="" />
          </div>
          {label && label}
        </label>
        {/* https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label#accessibility_concerns */}
        {children}
      </div>
    </div>
  );
};
