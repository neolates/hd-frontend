"use client";

import clsx from "clsx";
import Image from "next/image";
import {
  DetailedHTMLProps,
  InputHTMLAttributes,
  useEffect,
  useId,
  useMemo,
  useRef,
  useState,
} from "react";
import { countries, getCountryFlag, useTelephone } from "use-telephone";
import ArrowDown from "@assets/arrow.svg";
// eslint-disable-next-line boundaries/element-types
import { useIsClient } from "@/app/hooks/useIsClient";
// eslint-disable-next-line boundaries/element-types
import { useOutsideClick } from "@/app/hooks/useOutsideClick";
import "./PhoneField.scss";
import { PropsWithLabel } from "./types";

export type PhoneFieldProps = PropsWithLabel<
  DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>
>;

export const PhoneField = ({ label, className, value, onChange, ...props }: PhoneFieldProps) => {
  let inputId = useId();
  if (props.id) {
    inputId = props.id;
  }
  const tel = useTelephone({ initialValue: value ? (value as string) : "" });
  const isClient = useIsClient();
  const [isSelectVisible, setSelectVisible] = useState(false);
  const [countrySearch, setCountrySearch] = useState("");
  const [countryCode, setCountryCode] = useState("");
  const selectRef = useRef<HTMLDivElement>(null);
  const outsideClickHandler = () => setSelectVisible(false);
  useOutsideClick(selectRef, outsideClickHandler);

  useEffect(() => {
    const parsedNumber = tel.value.split(" ");
    if (parsedNumber.length > 0) {
      setCountryCode(parsedNumber[0]);
    }
  }, [tel]);

  const filteredCountries = useMemo(
    () =>
      countries
        .filter((country) => country.name.toLowerCase().includes(countrySearch.toLowerCase()))
        .map((country) => (
          <div
            key={country.code}
            onClick={() => {
              tel.onChangeCountry(country.code);
              setSelectVisible(false);
              setCountrySearch("");
            }}
            className="phone-field__select-option">
            {isClient && country.name}
          </div>
        )),
    [countrySearch, isClient, tel]
  );

  return (
    <div className={clsx("phone-field", className, { active: isSelectVisible })}>
      <label htmlFor={inputId} className="phone-field__label">
        {label}
      </label>

      <div className="phone-field__input-wrap">
        <div className="phone-field__country-select" ref={selectRef}>
          <div
            className="phone-field__country-code"
            onClick={() => setSelectVisible(!isSelectVisible)}>
            {countryCode ? (
              <Image
                src={getCountryFlag(tel.country)}
                width={20}
                height={14}
                alt={`flag-${tel.country}`}
                className="phone-field__flag"
              />
            ) : (
              <div style={{ width: "20px", height: "14px" }} />
            )}
            {countryCode}
            <Image
              src={ArrowDown}
              alt={"arrow"}
              width={2.5}
              height={4}
              className="phone-field__arrow"
            />
          </div>
          <div className={clsx("phone-field__options-wrap", { open: isSelectVisible })}>
            <input
              placeholder="search country"
              type="text"
              value={countrySearch}
              onChange={({ target: { value } }) => setCountrySearch(value)}
              className="phone-field__search"
            />
            {filteredCountries.length === 0 && (
              <div className="px-2 whitespace-nowrap">No countries with this name</div>
            )}
            {filteredCountries}
          </div>
        </div>
        <div className="phone-field__delimiter" />
        <input
          type="text"
          value={tel.value}
          onChange={(e) => {
            if (e.target.value.length >= countryCode.length && countryCode) {
              tel.onChange(e);
              onChange && onChange(e);
            }
          }}
          id={inputId}
          className="phone-field__input"
          placeholder="000 00 00 000"
        />
      </div>
    </div>
  );
};
