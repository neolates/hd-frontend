"use client";

import { ReactNode, useId, useState } from "react";

import BarLoader from "react-spinners/BarLoader";

import { TextField, TextFieldProps } from "./TextField";

import { PropsWithOptions } from "./types";

export interface AutocompleteFieldProps extends PropsWithOptions<TextFieldProps> {
  onSearch: (v: string) => Promise<any>;
  emptyListElement?: ReactNode;
}

import "./AutocompleteField.scss";

export const AutocompleteField = ({
  emptyListElement = <>No items found</>,
  onSearch,
  options,
  ...textFieldProps
}: AutocompleteFieldProps) => {
  let id = useId();
  if (textFieldProps.id) {
    id = textFieldProps.id;
  }
  const [isSearchPending, setSearchPending] = useState(false);

  const handleSearch = (v: string) => {
    setSearchPending(true);
    onSearch(v).then(() => {
      setSearchPending(false);
    });
  };

  return (
    <div className="autocomplete-field">
      <TextField
        {...textFieldProps}
        type="text"
        id={id}
        role="combobox"
        aria-haspopup="listbox"
        aria-owns={`suggestion-list-${id}`}
        aria-autocomplete="list"
        aria-expanded="false"
        aria-activedescendant=""
        onChange={(e) => handleSearch(e.target.value)}
      />
      {isSearchPending && (
        <BarLoader
          className="bar-loader"
          width={"100%"}
          height={2}
          color="#713fd0"
          speedMultiplier={0.5}
        />
      )}
      <ul
        className="suggestions z-10"
        id={`suggestion-list-${id}`}
        role="listbox"
        aria-labelledby={`${id}-label`}>
        {!options ||
          (options.length === 0 && (
            <li className="suggestions-option suggestions-option--empty">{emptyListElement}</li>
          ))}
        {options &&
          options.length > 0 &&
          options.map((option) => (
            <li
              key={option.value}
              className="suggestions-option"
              id={`option-${id}-${option.value}`}
              role="option"
              aria-selected="false">
              {option.text}
            </li>
          ))}
      </ul>
    </div>
  );
};
