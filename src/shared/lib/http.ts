export type ApiResponse<T, E = string> =
  | {
      data?: T;
      error?: E;
    }
  | undefined;

export function getBaseApiUrl(): string {
  return process.env.VERCEL_ENV == "preview" || process.env.VERCEL_ENV == "production"
    ? `https://${process.env.VERCEL_URL ?? process.env.BASE_URL}`
    : process.env.BASE_URL
    ? `https://${process.env.BASE_URL}`
    : "http://localhost:3000";
}

export async function fetcher<T>(arg: any, ...args: any): Promise<T> {
  const res = await fetch(arg, ...args);
  if (!res.ok) throw new Error("Failed to fetch");
  return res.json().then((res: ApiResponse<T>) => res?.data as T);
}
