const defaultLocale = "en";
const locales = [defaultLocale, "ru"];

export { defaultLocale, locales };
