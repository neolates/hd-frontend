// Compilation of:
// - https://github.com/prisma/prisma-client-extension-starter
// - https://github.com/dthyresson/prisma-extension-supabase-rls

import { Prisma } from "@prisma/client";

export interface SupabaseRLS {
  /**
   * The client extension name
   * @default 'prisma-extension-supabase-rls'
   */
  name?: string;
  /**
   * The name of the Postgres setting to use for the claims, `request.jwt.claims` by default.
   * Supabase sets using set_config() and get with current_setting()
   *
   * @default 'request.jwt.claims'
   */
  claimsSetting?: string;
  /**
   * A function that returns the JWT claims to use for the current request decoded from the Supabase access token.
   *
   * E.g.:
   *
   * {
   *   "aud": "authenticated",
   *   "exp": 1675711033,
   *   "sub": "00000000-0000-0000-0000-000000000000",
   *   "email": "user@example.com",
   *   "phone": "",
   *   "app_metadata": {
   *     "provider": "email",
   *     "providers": [
   *       "email"
   *     ]
   *   },
   *   "user_metadata": {},
   *   "role": "authenticated",
   *   "aal": "aal1",
   *   "amr": [
   *     {
   *       "method": "otp",
   *       "timestamp": 1675696651
   *     }
   *   ],
   *   "session_id": "000000000000-0000-0000-0000-000000000000"
   *  }
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  claimsFn?: undefined | (() => Record<string, any>);
  /**
   * The error to throw when the policy check fails.
   */
  policyError?: Error;
  /**
   * Log errors to the console.
   * @default false
   */
  logging?: boolean;
}

const defaultSupabaseRLS: SupabaseRLS = {
  name: "prisma-extension-supabase-rls",
  claimsSetting: "request.jwt.claims",
  claimsFn: undefined,
  policyError: new Error("Not authorized."),
  logging: false,
};

export const applySupabaseRLS = (_extensionArgs: SupabaseRLS = defaultSupabaseRLS) => {
  const name = _extensionArgs.name || defaultSupabaseRLS.name;
  const claimsFn = _extensionArgs.claimsFn || defaultSupabaseRLS.claimsFn;
  const claimsSetting = _extensionArgs.claimsSetting || defaultSupabaseRLS.claimsSetting;
  const policyError = _extensionArgs.policyError || defaultSupabaseRLS.policyError;
  const logging = _extensionArgs.logging || false;

  return Prisma.defineExtension({
    name,
    model: {
      $allModels: {
        async $allOperations({ args, query }) {
          const claims = claimsFn ? JSON.stringify(claimsFn() || {}) : "";
          const ctx = Prisma.getExtensionContext(this);
          try {
            const [, result] = await ctx.$transaction([
              ctx.$executeRaw`SELECT set_config(${claimsSetting}, ${claims}, TRUE)`,
              query(args),
            ]);

            return result;
          } catch (e) {
            if (logging) console.error(e);
            throw policyError || e;
          }
        },
      },
    },
  });
};
