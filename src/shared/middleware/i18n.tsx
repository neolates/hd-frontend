import { match as matchLocale } from "@formatjs/intl-localematcher";
import Negotiator from "negotiator";
import type { NextRequest } from "next/server";
import { NextResponse } from "next/server";
import { i18n } from "@/shared/lib";

function getLocale(request: NextRequest): string | undefined {
  // Negotiator expects plain object so we need to transform headers
  const negotiatorHeaders: Record<string, string> = {};
  request.headers.forEach((value, key) => (negotiatorHeaders[key] = value));

  // Use negotiator and intl-localematcher to get best locale
  const languages = new Negotiator({ headers: negotiatorHeaders }).languages();
  const locales: string[] = i18n.locales.slice();

  return matchLocale(languages, locales, i18n.defaultLocale);
}

export function i18nMiddleware(request: NextRequest) {
  // Skip next internal and image requests
  if (
    request.nextUrl.pathname.startsWith("/_next") ||
    request.nextUrl.pathname.includes("/api/") ||
    /\.(.*)$/.test(request.nextUrl.pathname) ||
    new RegExp(`${i18n.locales.map((v) => `^/${v}`).join("|")}`).test(request.nextUrl.pathname)
  ) {
    return;
  }

  let pathname = request.nextUrl.pathname;
  if (pathname.startsWith("/")) {
    pathname = pathname.substring(1);
  }
  if (pathname.endsWith("/")) {
    pathname = pathname.substring(0, -1);
  }
  pathname = [getLocale(request), pathname].filter(Boolean).join("/");

  return NextResponse.redirect(new URL(`/${pathname}${request.nextUrl.search}`, request.url), 302);
}

export const config = {
  // Matcher ignoring `/_next/` and `/api/`
  matcher: ["/((?!api|_next/static|_next/image).*)"],
};
