import Image from "next/image";

import React from "react";

/* eslint-disable */
import picture1 from "@assets/bodygraph/1.svg";
import picture1Blue from "@assets/bodygraph/1-blue.svg";
import picture2 from "@assets/bodygraph/2.svg";
import picture2Blue from "@assets/bodygraph/2-blue.svg";
import picture3 from "@assets/bodygraph/3.svg";
import picture3Blue from "@assets/bodygraph/3-blue.svg";
import picture4 from "@assets/bodygraph/4.svg";
import picture4Blue from "@assets/bodygraph/4-blue.svg";
import picture5 from "@assets/bodygraph/5.svg";
import picture5Blue from "@assets/bodygraph/5-blue.svg";
import picture6 from "@assets/bodygraph/6.svg";
import picture6Blue from "@assets/bodygraph/6-blue.svg";
import picture7 from "@assets/bodygraph/7.svg";
import picture7Blue from "@assets/bodygraph/7-blue.svg";
import picture8 from "@assets/bodygraph/8.svg";
import picture8Blue from "@assets/bodygraph/8-blue.svg";
import picture9 from "@assets/bodygraph/9.svg";
import picture9Blue from "@assets/bodygraph/9-blue.svg";
import picture10 from "@assets/bodygraph/10.svg";
import picture10Blue from "@assets/bodygraph/10-blue.svg";
import picture11 from "@assets/bodygraph/11.svg";
import picture11Blue from "@assets/bodygraph/11-blue.svg";
import picture13 from "@assets/bodygraph/13.svg";
import picture13Blue from "@assets/bodygraph/13-blue.svg";
import picture14 from "@assets/bodygraph/14.svg";
import picture14Blue from "@assets/bodygraph/14-blue.svg";
import arrowDownBlue from "@assets/bodygraph/arrow-down-blue.svg";
import arrowUpBlue from "@assets/bodygraph/arrow-up-blue.svg";
import arrowDownYellow from "@assets/bodygraph/arrow-down-yellow.svg";
import arrowUpYellow from "@assets/bodygraph/arrow-up-yellow.svg";
import starYellow from "@assets/bodygraph/star-yellow.svg";
/* eslint-enable */

import "./table.scss";

type TableProps = {
  mode: "design" | "personality";
  data: Array<number>;
};

const Table = ({ mode, data }: TableProps) => {
  const images = [
    {
      design: { icon: picture1, status: arrowUpYellow },
      personality: { icon: picture1Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture2, status: arrowDownYellow },
      personality: { icon: picture2Blue, status: arrowDownBlue },
    },
    {
      design: { icon: picture3, status: arrowUpYellow },
      personality: { icon: picture3Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture4, status: arrowDownYellow },
      personality: { icon: picture4Blue, status: arrowDownBlue },
    },
    {
      design: { icon: picture5, status: starYellow },
      personality: { icon: picture5Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture6, status: starYellow },
      personality: { icon: picture6Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture7, status: arrowUpYellow },
      personality: { icon: picture7Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture8, status: arrowUpYellow },
      personality: { icon: picture8Blue, status: arrowDownBlue },
    },
    {
      design: { icon: picture9, status: arrowUpYellow },
      personality: { icon: picture9Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture10, status: arrowUpYellow },
      personality: { icon: picture10Blue, status: arrowDownBlue },
    },
    {
      design: { icon: picture11, status: arrowUpYellow },
      personality: { icon: picture11Blue, status: arrowDownBlue },
    },
    {
      design: { icon: picture13, status: arrowUpYellow },
      personality: { icon: picture13Blue, status: arrowUpBlue },
    },
    {
      design: { icon: picture14, status: arrowUpYellow },
      personality: { icon: picture14Blue, status: arrowUpBlue },
    },
  ];

  return (
    <div className="bodygraph-table flex-1">
      {data.map((item, index) => (
        <div className="flex items-center justify-between gap-2 stats-row-table" key={index}>
          <Image src={images[index][mode]["icon"]} alt={index + mode + "icon"} />
          <span>{item}</span>
          <Image src={images[index][mode]["status"]} alt={index + mode + "status"} />
        </div>
      ))}
    </div>
  );
};

export default Table;
