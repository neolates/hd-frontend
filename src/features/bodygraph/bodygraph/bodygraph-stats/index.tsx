import React, { ReactElement } from "react";

import Pagination from "./Pagination";
import Table from "./Table";

import "./stats.scss";

type StatsProps = {
  mode: "design" | "personality";
  color: "#FFBC40" | "#3EC6FF";
};

const BodyGraphStats = ({ mode, color }: StatsProps): ReactElement => {
  const DesignMode = () => (
    <div style={{ color }} className="bodygraph-stats flex-wrap sm:flex-nowrap flex gap-3">
      <span className="block title-person-design w-full sm:w-auto">DESIGN</span>
      <div className="flex gap-3">
        <Table
          mode="design"
          data={[43.1, 23.1, 42.4, 49.3, 4.3, 43.1, 58.3, 28.4, 39.2, 38.1, 10.5, 38.1, 1.3]}
        />
        <Pagination color={color} side="right" />
      </div>
    </div>
  );

  const PersonalityMode = () => (
    <div
      style={{ color }}
      className="bodygraph-stats flex flex-wrap sm:flex-nowrap justify-end sm:justify-start gap-3">
      <div className="flex gap-3 order-2 sm:order-none">
        <Pagination color={color} side="left" />
        <Table
          mode="personality"
          data={[43.1, 23.1, 42.4, 49.3, 4.3, 43.1, 58.3, 28.4, 39.2, 38.1, 10.5, 38.1, 1.3]}
        />
      </div>
      <span className="block title-person-personality w-full sm:w-auto">PERSONALITY</span>
    </div>
  );

  return (
    <div className={`font-medium text-xs`}>
      {mode === "design" ? <DesignMode /> : <PersonalityMode />}
    </div>
  );
};

export default BodyGraphStats;
