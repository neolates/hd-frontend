import Image from "next/image";

import React, { FC } from "react";

/* eslint-disable */
import arrowBlueLeft from "@assets/bodygraph/arrow-blue-left.svg";
import arrowBlueRight from "@assets/bodygraph/arrow-blue-right.svg";
import arrowYellowLeft from "@assets/bodygraph/arrow-yellow-left.svg";
import arrowYellowRight from "@assets/bodygraph/arrow-yellow-right.svg";
/* eslint-enable */

import "./pagination.scss";

type StatsPaginationProps = {
  side: "left" | "right";
  color: "#FFBC40" | "#3EC6FF";
};

const Pagination: FC<StatsPaginationProps> = ({ side, color }) => {
  return (
    <div className="bodygraph-pagination">
      <div className="mb-2">Color Tone</div>
      <div className="flex items-center gap-2 mb-2">
        {side === "left" && <div className="main-text-color stats-pagination-text">1/3</div>}
        <button className="stats-btn">
          <Image src={color === "#FFBC40" ? arrowYellowLeft : arrowBlueLeft} alt="Button Icon" />
        </button>
        {side === "right" && <div className="main-text-color stats-pagination-text">1/3</div>}
      </div>
      <div className="flex items-center gap-2">
        {side === "left" && <div className="main-text-color stats-pagination-text">3/5</div>}
        <button className="stats-btn">
          <Image src={color === "#FFBC40" ? arrowYellowRight : arrowBlueRight} alt="Button Icon" />
        </button>
        {side === "right" && <div className="main-text-color stats-pagination-text">3/5</div>}
      </div>
    </div>
  );
};

export default Pagination;
