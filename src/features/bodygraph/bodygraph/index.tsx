import React, { ReactElement } from "react";

import BodyGraphStats from "./bodygraph-stats";
import Graph from "./Graph";

import "./bodygraph.scss";

const BodyGraph = (): ReactElement => {
  return (
    <div className="bodygraph relative w-full">
      <div className="w-full flex justify-between md:absolute">
        <BodyGraphStats mode="design" color="#FFBC40" />
        <BodyGraphStats mode="personality" color="#3EC6FF" />
      </div>
      <div className="bodygraph-container">
        <Graph />
      </div>
    </div>
  );
};

export default BodyGraph;
