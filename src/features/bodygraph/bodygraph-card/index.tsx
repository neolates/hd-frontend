import Image, { StaticImageData } from "next/image";

import React, { FC } from "react";

import "./bodygraph-card.scss";

type BodyGraphCardProps = {
  text: string;
  icon: StaticImageData;
  status: string;
  btnTitle: string;
  className: string;
};

const BodyGraphCard: FC<BodyGraphCardProps> = ({ text, icon, status, btnTitle, className }) => {
  return (
    <div
      className={`bodygraph-card pt-2 sm:pt-4 px-2 pb-2 flex flex-col items-center relative justify-stretch w-full h-full ${className}`}>
      <div className="bodygraph-card-icon mb-32px relative z-10">
        <Image src={icon} alt={text} />
      </div>
      <h4 className="mb-auto relative z-10 text-center">{text}</h4>
      <div className="description-text-color mb-auto sm:mb-20px relative z-10">{status}</div>
      <button className="bodygraph-card-btn text-sm leading-16 w-full relative z-10">
        {btnTitle}
      </button>
    </div>
  );
};

export default BodyGraphCard;
