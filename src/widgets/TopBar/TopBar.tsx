"use client";

import clsx from "clsx";
import Image, { StaticImageData } from "next/image";
import { FC } from "react";
import { Button } from "@/shared/ui";

import "./top-bar.scss";

type TopBarProps = {
  className?: string;
  onLogo?: () => void;
  isSignedIn: boolean;
  avatarSrc?: StaticImageData;
  userName?: string;
  onSignUp?: () => void;
  onLogIn?: () => void;
};

export const TopBar: FC<TopBarProps> = ({
  className,
  onSignUp,
  userName,
  isSignedIn,
  avatarSrc,
  onLogIn,
  onLogo,
}) => {
  const _className = clsx(className, "top-bar");
  return (
    <header className={_className}>
      <div className="top-bar__content-wrapper">
        <div className="top-bar__start">
          <div onClick={onLogo} className="cursor-pointer">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="50"
              height="50"
              viewBox="0 0 50 50"
              fill="none">
              <circle cx="25" cy="25" r="24" stroke="white" stroke-width="2" />
              <path d="M6.61803 39L25 2.23607L43.382 39H6.61803Z" stroke="white" stroke-width="2" />
              <path
                d="M37.2482 27.4683C35.2799 21.1252 28.5439 17.5771 22.1997 19.5415C17.1249 21.1158 14.2862 26.5049 15.8582 31.5803C16.4621 33.5308 17.8163 35.1614 19.6228 36.113C21.4293 37.0647 23.54 37.2594 25.4901 36.6543C28.7371 35.6464 30.5535 32.1984 29.5485 28.9505C29.1623 27.7019 28.2958 26.6581 27.1398 26.0487C25.9837 25.4393 24.6328 25.3143 23.3846 25.7013C22.3812 26.011 21.5429 26.7082 21.0555 27.6382C20.568 28.5682 20.4716 29.6542 20.7877 30.6555C21.3079 32.3131 23.0718 33.2367 24.7305 32.7198C26.0556 32.3026 26.7939 30.8924 26.3819 29.5656C26.0512 28.5008 24.92 27.9056 23.8552 28.2362C23.4464 28.3626 23.2173 28.5717 23.0182 28.9505C22.8191 29.3292 22.8021 30.0185 23.1818 30.3398"
                stroke="white"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
        </div>

        <div className="top-bar__activities">
          {!isSignedIn ? (
            <div className="top-bar__log-in-wrapper">
              <Button onClick={onLogIn} className="top-bar__log-in-button" size="sm">
                Log in
              </Button>
              <Button onClick={onSignUp} className="top-bar__sign-in-button" size="sm">
                Sign Up
              </Button>
            </div>
          ) : (
            <div className="top-bar__signed-in-user-wrapper">
              <p className="top-bar__user-name">{userName}</p>
              <Image
                className="top-bar__user-avatar"
                src={avatarSrc ?? ""}
                width={50}
                height={50}
                alt="user avatart"
              />
            </div>
          )}
        </div>
      </div>
    </header>
  );
};
