import { fetchChart } from "./api";

async function Chart({ chartKey }: { chartKey: string }) {
  const data = await fetchChart(chartKey);
  return <div className="text-white">Definition: {data.data.interpretation.definition}</div>;
}

export default Chart;
