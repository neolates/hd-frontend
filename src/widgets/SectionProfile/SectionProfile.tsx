'use client';
import { Button } from '@/shared/ui';
import Image from 'next/image';

import profileBg from '@assets/profile-section-bg.svg'

import './style.scss'


type TSectionProfile = {
  hrefLists: TItem[][],
}

type TItem = {
  lable: string,
  href?: string,
  onClick?: () => void
}

export const SectionProfile = ({ hrefLists }: TSectionProfile) => {
  return (
    <div
      className="section-profile section"
    >
      <div
        className="section-profile--card"
      >
        <div
          className='section-profile--image-container'
        >
          <Image
            src={profileBg}
            className='section-profile--image'
            alt=''
          />
        </div>
        <div
          className="section-profile--header"
        >
          <h1
            className="section-profile--title"
          >
            Profiles in Human Design
          </h1>
          <p
            className="section-profile--subtitle"
          >
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
          </p>
        </div>
        <div
          className='section-profile--body'
        >
          {hrefLists.map(list => (
            <div
              className="section-profile--list"
            >
              {list.map(item => (
                <div
                  className="section-profile--item"
                >
                  <Button
                    href={item?.href ?? ''}
                    onClick={item.onClick}
                    className='section-profile--button'
                  >
                    {item.lable}
                  </Button>
                </div>
              ))}
            </div>
          ))}

        </div>
      </div>
    </div>
  );
};


