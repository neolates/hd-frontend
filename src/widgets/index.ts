import Footer from "./layouts/widgets/Footer";
import Header from "./layouts/widgets/Header";

export { Footer, Header };
export { TopBar } from './TopBar'
export { SectionProfile } from './SectionProfile'
