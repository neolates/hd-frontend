"use client";
import React, { ReactNode } from "react";
import Footer from "./widgets/Footer";
import Header from "./widgets/Header";

interface BaseLayoutProps {
  children: ReactNode;
}

const BaseLayout = ({ children }: BaseLayoutProps): JSX.Element => {
  return (
    <>
      <Header />
      <main className="w-full sm:mx-auto sm:container md:px-4">{children}</main>
      <Footer />
    </>
  );
};

export default BaseLayout;
