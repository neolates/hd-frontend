import Image from "next/image";

import React from "react";

import "./Footer.scss";

function Footer(): JSX.Element {
  const currentYear = new Date().getFullYear();

  const socialMediaImages = [
    { src: "/assets/social/facebook.svg", alt: "Facebook" },
    { src: "/assets/social/instagram.svg", alt: "Instagram" },
    { src: "/assets/social/twitter.svg", alt: "Twitter" },
    { src: "/assets/social/youtube.svg", alt: "Youtube" },
  ];

  return (
    <footer className="site-footer container mx-auto md:px-4">
      <ul className="no-bullets row">
        <li className="site-footer-lang">
          <Image
            className="lang"
            src="/assets/lang/lang-en.svg"
            width="19"
            height="14"
            alt="Language"
          />
          <span className="text-btn text-white">English</span>
        </li>
        <li className="site-footer-delimiter mx-2"></li>
        {socialMediaImages.map((image, index) => (
          <li key={index} className="row">
            <Image className="social-icon" width="30" height="30" src={image.src} alt={image.alt} />
          </li>
        ))}
        <li className="ml-2">
          <Image
            className="social-icon--appStore"
            width="118"
            height="38"
            src="/assets/appstorelogo.svg"
            alt="App Store"
          />
        </li>
      </ul>

      <ul className="no-bullets">
        <li className="row">
          <p className="link mr-1">Terms of Use</p>
          <p className="link mr-1">Privacy Notice</p>
          <p className="link mr-1">Cookie Preferance</p>
        </li>
        <li className="site-footer-copyright">© 2017 - {currentYear} Human Design X</li>
      </ul>
    </footer>
  );
}

export default Footer;
