"use client";

import Link from "next/link";
import Image from "next/image";

import React, { ReactElement, useState } from "react";

/* eslint-disable */
import logo from "@assets/logo.png";
import avatar from "@assets/avatar.png";
import arrow from "@assets/arrow.svg";
/* eslint-enable */

import { Button } from "@/shared/ui";

import "./Header.scss";

function Header(): ReactElement {
  const [showUserHeader, setShowUserHeader] = useState(false);
  const [, setUserName] = useState("");

  const handleButtonClick = () => {
    setShowUserHeader(true);
    setUserName("John Doe"); // Replace with the user's name retrieved from the login process
  };

  return (
    <header className="default container mx-auto md:px-4">
      <nav>
        <ul className="flex items-center text-sm leading-[16px]">
          <li>
            <Image className="logo" src={logo} alt="Logo" />
          </li>
          {showUserHeader ? (
            <>
              <li>
                <p className="text">&gt; Back</p>
              </li>
              <li>
                <p className="t-bobby"> | View chart Boby Sid</p>
              </li>
              <li>
                <p className="username">Boby Sid</p>
              </li>
              <li>
                <Image className="profile-picture" src={avatar} alt="Profile Picture" />
              </li>
            </>
          ) : (
            <>
              <li className="sm:flex-1 ml-1">
                <Link href={"/"}>
                  <Image className=" w-[5px] h-2 mr-1" alt="arrow" src={arrow} />
                  <span className="font-semibold">Back to Calculate</span>
                </Link>
              </li>
              <li className="flex flex-row items-start justify-start gap-[10px] text-right">
                <Button size="sm" variant="text" onClick={handleButtonClick}>
                  Log in
                </Button>
                <Button size="sm" variant="outline">
                  Sign Up
                </Button>
              </li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
}

export default Header;
