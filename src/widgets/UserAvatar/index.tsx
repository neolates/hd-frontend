import Image, { StaticImageData } from "next/image";

import React, { CSSProperties, FC } from "react";

import "./user-avatar.scss";

type UserAvatarProps = {
  src: StaticImageData;
  className?: string;
  width?: number;
  height?: number;
  alt: string;
  style: CSSProperties;
};

const UserAvatar: FC<UserAvatarProps> = ({ src, width, height, className = "", style }) => {
  return (
    <Image
      src={src}
      alt="user avatar"
      width={width}
      height={height}
      className={`user-avatar ${className}`}
      style={{
        borderWidth: 2,
        borderColor: "rgba(20, 20, 20, 1)",
        ...style,
      }}
    />
  );
};

export default UserAvatar;
