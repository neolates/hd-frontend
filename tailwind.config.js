/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      container: {
        center: true, // Center the content
        padding: "20px", // Add some padding on the sides
        screens: {
          sm: "100%", // Full width on small screens
          md: "100%", // Full width on medium screens
          lg: "1024px", // Max-width on large screens
          xl: "1240px", // Max-width on extra large screens
        },
      },
      lineHeight: {
        16: "16px",
        32: "32px",
        40: "42px",
      },
      spacing: {
        2: "10px",
        "6px": "6px",
        "20px": "20px",
        "32px": "32px",
        12: "60px",
      },
      colors: {
        gray: {
          100: "#909090",
          200: "#2b2b2b",
          300: "#2a2a2a",
          400: "#1a1a1a",
          500: "rgba(255, 255, 255, 0.1)",
          600: "rgba(12, 12, 12, 0.96)",
        },
        "global-btn-text-white": "#fff",
        darkslategray: {
          100: "#2f2f2f",
          200: "#2d2d2d",
        },
        dimgray: {
          100: "#646464",
          200: "#615d66",
        },
        silver: "#bbb",
        "text-description": "#696969",
        whitesmoke: "#f1f1f1",
        "global-btn-text": "#0c0c0c",
        slateblue: "#58379d",
        deepskyblue: "#3ec6ff",
        goldenrod: "#ffbc40",
        "text-main-text": "#e7e7e7",
        "primary-purple": "#713FD0",
      },
      fontFamily: {
        "global-btn-text": "Poppins",
      },
      borderRadius: {
        mini: "15px",
        xl: "20px",
        "6xl": "25px",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic": "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontSize: {
        sm: "14px",
        smi: "13px",
        md: "14px",
        lg: "18px",
        xs: "12px",
        "4xs": "9px",
        base: "16px",
        xxl: "36px",
        "41xl": "60px",
      },
    },
  },
  plugins: [],
  corePlugins: {
    preflight: false,
  },
};
