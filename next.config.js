/** @type {import('next').NextConfig} */
const nextConfig = {
  eslint: {
    dirs: ["src/app", "src/domain", "src/features", "src/shared"],
  },
  experimental: {
    serverActions: true,
    appDir: true,
  },
};

module.exports = nextConfig;
